<?php
if (Auth::check()) {
    $user = Auth::user();
}
?>
        <!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="author" content="DINFO | Amapá Previdência - AMPREV">
    <meta name="description" content="Amprev - Amapá Previdência">
    <meta name="keywords" content="AMPREV - Amapá Previdência">
    <meta name="reply-to" content="dinfo@amprev.ap.gov.br">

    <title>{{ config('app.name', 'Laravel') }}</title>

    {{--ICONES--}}
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon-16x16.png') }}">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="manifest" href="{{ asset('manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
    {{--ICONES--}}

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fa.css') }}">
    <link rel="stylesheet" href="{{ asset('css/semsass.css') }}">

    @yield('css')

</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    @auth()
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('inicio') }}">
                                <i class="fa fa-users"></i>
                                Reunião
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{ route('arquivos') }}">
                                <i class="fa fa-archive"></i>
                                Arquivos
                            </a>
                        </li>
                        @if($user->nivel == 0)
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-cogs"></i>
                                    Administração <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#" id="distribuicao" data-toggle="modal" data-target="distModal">
                                        <i class="fa fa-random"></i>
                                        Distribuição
                                    </a>
                                    <a class="dropdown-item" href="{{ route('reunioes.index') }}">
                                        <i class="fa fa-globe"></i>
                                        Reuniões
                                    </a>
                                    <a class="dropdown-item" href="{{ route('entidades.index') }}">
                                        <i class="fa fa-university"></i>
                                        Entidades
                                    </a>
                                    <a class="dropdown-item" href="{{ route('usuarios.index') }}">
                                        <i class="fa fa-street-view"></i>
                                        Usuários
                                    </a>
                                </div>
                            </li>
                        @endif
                        <li class="nav-item">
                            <div class="btn-group">
                                <button type="button" class="btn btn-outline-secondary" disabled>
                                    @if($user->foto)
                                        <img src="{{ asset("arquivos/$user->foto") }}" style="max-width: 25px"
                                             class="rounded-circle">
                                    @else
                                        <img src="{{ asset('img/default-avatar.png') }}" style="max-width: 25px"
                                             class="rounded-circle">
                                    @endif
                                    {{ $user->name }}
                                </button>
                                <button type="button" class="btn btn-outline-info dropdown-toggle dropdown-toggle-split"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="{{ route('perfil') }}">
                                        <i class="fa fa-id-card"></i>
                                        Meu Perfil</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out-alt"></i>
                                        Sair
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </li>
                    @endauth
                </ul>
            </div>
        </div>
    </nav>
    <div id="distModal" class="modal fade"></div>
    <main class="py-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    @if(Session::has('sucesso'))
                        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
                            <strong><i class="fa fa-check"></i> {{ Session::get('sucesso') }}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    @if(Session::has('invalido'))
                        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
                            <strong><i class="fa fa-ban"></i> {{ Session::get('invalido') }}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    @if($errors)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>{{ $error }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Fechar">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>

        @yield('content')
    </main>
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script>
    $('#distribuicao').on('click', function () {
        let divModal = $('#distModal');
        let rota = "/admin/distribuicao/";

        $.ajax({
            url: rota,
            error: function (err, xhr) {
                console.log(xhr)
            },
            success: function (data) {
                divModal.html(data);
                divModal.modal('toggle')
            }
        })
    });
</script>
@stack('js')

</body>
</html>
