<li class="nav-item text-center list-group-item active">
    <h1>Pautas</h1>
</li>
@foreach($reuniao->pautas as $pauta)
    <li class="nav-item list-group-item ">
        @if(count($pauta->items))
            <a class="btn btn-link nav-link" data-toggle="collapse"
               data-target="#pauta{!! $pauta->id !!}" style="text-align: left">
                <span class="reuniao-item"><i class="fa fa-fw fa-chevron-circle-down"></i></span>
                {{ $pauta->nome }}
            </a>
        @else
            <span class="btn" style="text-align: left">
                <span class="reuniao-item"><i class="fa fa-dot-circle"></i></span>
                {{ $pauta->nome }}
            </span>
        @endif
        @if(Auth::user()->nivel == 0)
            <a data-id="{{$pauta->id}}" class="btn text-white btn-sm btn-primary addItem"><i
                        class="fa fa-plus fa-sm"></i> Add Item</a>
            <a data-id="{{$pauta->id}}" class="btn text-white btn-sm btn-danger delPauta"><i
                        class="fa fa-ban fa-sm"></i> Remover</a>
        @endif
    </li>

    @if($pauta->items)
        <div class="collapse" id="pauta{!! $pauta->id !!}"
             data-parent="#accordAndamento">
            <ul class="nav list-group flex-column">
                @foreach($pauta->items as $item)
                    @php($status = $item->status == 'Aberta' ? 'btn-aberto' : 'btn-fechado')
                    <li class="nav-item list-group-item"
                        @if($item->status != 'Aberta') style='background-color: #5552;' @endif>
                        <div class="btn btn-link nav-link text-left {{ $item->status == 'Aberta' ? 'aberto' : 'fechado' }}"
                             data-toggle="collapse"
                             data-target="#texto{!! $item->id !!}">
                            @if(Auth::user()->nivel == 0)
                                @if($item->status == 'Aberta')
                                    <button class="btn btn-sm btn-danger {{ $status }}" data-id="{{ $item->id }}">
                                        <i class="fa fa-fw fa-lock" title="Encerrar"></i>
                                    </button>
                                @elseif($item->status == 'Fechada')
                                    <button class="btn btn-sm btn-success {{ $status }}" data-id="{{ $item->id }}">
                                        <i class="fa fa-fw fa-lock-open" title="Liberar Item"></i>
                                    </button>
                                @elseif($item->status == 'Encerrada')
                                    <button class="btn btn-sm btn-warning {{ $status }}" data-id="{{ $item->id }}">
                                        <i class="fa fa-fw fa-exclamation-circle" title="Encerrada"></i>
                                    </button>
                                @endif
                            @endif
                            {{ $item->nome }}
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
@endforeach

<script>

    setTimeout(function (e) {
        let delPauta = $('.delPauta');
        delPauta.on('click', function () {
            let dpId = $(this).data('id');
            $.ajax({
                url: '/delPauta/' + dpId,
                success: function (data) {
                    console.log(data)
                },
                error: function (err, xhr) {
                    console.log(err)
                }
            })
        });


        let addItem = $('.addItem');
        let addItemDiv = $('#addItemDiv');

        addItem.on('click', function () {
            let addId = $(this).data('id');
            $.ajax({
                url: '/addItemView/' + addId,
                error: function (err, xhr) {
                    console.log(xhr)
                },
                success: function (data) {
                    addItemDiv.html(data);
                    addItemDiv.modal('toggle');
                },
            })
        });
    }, 2000);
</script>

@if(Auth::user()->nivel == 0)
    <script>
        setTimeout(function (e) {
            let openItem = $('.btn-fechado');
            openItem.on('click', function () {
                let oiId = $(this).data('id');
                $.ajax({
                    url: '/liberarItem/' + oiId,
                    error: function (err, xhr) {
                        $(this).removeClass('fechado');
                        $(this).addClass('aberto')
                    },
                    success: function (data) {
                        loadPauta();
                        $(this).removeClass('fechado');
                        $(this).addClass('aberto')
                    }
                })
            });

            let fecharItem = $('.btn-aberto');
            fecharItem.on('click', function () {
                let oiId = $(this).data('id');
                $.ajax({
                    url: '/encerrarItem/' + oiId,
                    error: function (err, xhr) {

                    },
                    success: function (data) {
                        loadPauta();
                        $(this).removeClass('aberto');
                        $(this).addClass('encerrado')
                    }
                })
            });
        }, 2000)
    </script>
@endif