@foreach($reunioes as $reuniao)
    <div class="row">
        @php($status = $reuniao->status == \App\Models\Reuniao::STATUS_ABERTO)

        <div class="col-12 mb-2">
            <div class="card text-center {{ $status ? 'border-primary' : 'border-success' }}">
                <div class="card-header {{ $status ? 'text-primary' : 'text-muted' }}">
                    @if($status)
                        <p style="margin: 0;">Aberta</p>
                    @else
                        <p style="margin: 0;">Fechada</p>
                    @endif
                </div>
                <div class="card-body {{ $status ? 'text-primary' : 'text-success' }}">
                    <h4>
                        {{ $reuniao->descricao }}</h4>
                    <div class="text-muted">
                        <i class="fa fa-calendar"></i>
                        {{ $reuniao->dataExtensa($reuniao->created_at) }}
                    </div>
                </div>
                <div class="card-footer">
                    @if($status)
                        <a class="btn btn-primary" href="/reunioes/participar/{!! $reuniao->id !!}">
                            Entrar
                            <i class="fa fa-arrow-right"></i>
                        </a>
                    @else
                        @if($reuniao->ata())
                            <p class="text-muted" style="margin: 0;">Reunião Encerrada</p>
                            <button class="btn btn-outline-secondary">
                                <i class="fa fa-search"></i>
                                Visualizar Ata
                            </button>
                        @elseif (Auth::user()->nivel == 0)
                            <a href="/reunioes/participar/{!! $reuniao->id !!}" class="btn btn-outline-info">
                                <i class="fa fa-globe"></i>
                                Gerenciar Reunião
                            </a>
                        @else
                            <p class="text-muted" style="margin: 0;">Aguardando...</p>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
@endforeach