<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header bg-info">
            <h5 class="modal-title text-white">Incluir Item na Pauta: {{ $pauta->nome }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="addItemForm" action="{{ route('itempauta.store') }}" class="needs-validation" novalidate
              method="POST" enctype="application/x-www-form-urlencoded">
            {!! csrf_field() !!}
            <div class="modal-body">
                <div class="form-row">
                    <div class="col-md-12 mb-3">
                        <input type="text" class="form-control" id="nome" name="nome" placeholder="Descrição"
                               required>
                        <div class="valid-feedback">
                            Ok !
                        </div>
                    </div>
                    <div class="col-md-12 mb-3">
                        <textarea class="form-control ckeditor" id="ckeditor-content" name="texto" placeholder="Texto"></textarea>
                        <div class="valid-feedback">
                            Ok !
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="pauta_id" value="{{ $pauta->id }}">
                <button type="submit" class="btn btn-primary">Adicionar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </form>
    </div>
</div>

<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script>
    jQuery(document).ready(function($) {
        $('.ckeditor').ckeditor({
            "filebrowserBrowseUrl": "{{ url('/admin/elfinder/ckeditor') }}",
            "extraPlugins" : 'oembed,widget'
        });
    });
</script>
<script src="{{ url('/vendor/colorbox/jquery.colorbox-min.js') }}"></script>
<script>
    $(document).on('click','.popup_selector[data-inputid=image-filemanager]',function (event) {
        event.preventDefault();
        var triggerUrl = "{{ url('/admin/elfinder/popup/image-filemanager') }}";
        $.colorbox({
            href: triggerUrl,
            fastIframe: true,
            iframe: true,
            width: '80%',
            height: '80%'
        });
    });

    function processSelectedFile(filePath, requestingField) {
        $('#' + requestingField).val(filePath.replace(/\\/g,"/"));
    }

    $(document).on('click','.clear_elfinder_picker[data-inputid=image-filemanager]',function (event) {
        event.preventDefault();
        var updateID = $(this).attr('data-inputid'); // Btn id clicked
        $("#"+updateID).val("");
    });
</script>