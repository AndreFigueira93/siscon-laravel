@foreach($reuniao->users as $presentes)
    <li class="list-inline-item">
        <button type="button" class="btn btn-outline-secondary" disabled>
            @if($presentes->foto)
                <img src="{{ asset("arquivos/$presentes->foto") }}" style="max-width: 25px"
                     class="rounded-circle">
            @else
                <img src="http://10.155.17.70:3000/img/default-avatar.png" style="max-width: 25px"
                     class="rounded-circle">
            @endif
            {{ $presentes->name }}
        </button>
    </li>
@endforeach