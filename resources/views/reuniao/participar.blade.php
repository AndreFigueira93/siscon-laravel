@extends('layouts.app')

@section('content')

    <nav class="navbar navbar-dark bg-dark flex-md-nowrap p-0">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Reunião: {{ $reuniao->descricao }}</a>
        <div id="timer-wrapper" class="timer-wrapper iniciar">
            <div id="reuniao-timer" class="reuniao-timer">
                {{--@if($reuniao->hora_encerramento)
                    {!!
                        gmdate('H:i:s', (\Carbon\Carbon::createFromFormat('Y-m-d H:i:s' , $reuniao->hora_encerramento))
                            ->diffInSeconds(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s' , $reuniao->hora_abertura))
                        )
                    !!}
                @else
                    00:00:00
                @endif--}}
                --:--:--
            </div>
            @if(Auth::user()->nivel == 0)
                <a id="iniciar-reuniao" class="btn btn-primary" href="#">Iniciar Reunião</a>
                <a id="finalizar-reuniao" class="btn btn-danger" href="#">Finalizar Reunião</a>
            @endif
        </div>
    </nav>

    <div class="container-fluid" id="reuniao-id" data-id="{{ $reuniao->id }}">
        <div class="row">
            <nav class="col-md-3 bg-light sidebar">
                <div class="sidebar-sticky">
                    @if(Auth::user()->nivel == 0)
                        <button class="btn btn-block btn-info mt-4 mb-1" id="addPauta">
                            <b>Adicionar Pauta</b>
                        </button>
                    @endif

                    <ul class="nav flex-column mt-4 mb-2 list-group" id="accordAndamento">
                        <li class="nav-item text-center list-group-item active">
                            <h1>Pautas</h1>
                        </li>

                        <div id="template-pauta" style="display:none">
                            <div>
                                <li class="nav-item list-group-item">
                                    <a class="btn btn-link nav-link" data-toggle="collapse" data-target="#pauta[id]"
                                       style="text-align: left">
                                        <span class="reuniao-item"><i
                                                    class="fa fa-fw fa-chevron-circle-down"></i></span>
                                        <span>[nome]</span>
                                        @if(Auth::user()->nivel == 0)
                                            <a data-id="[id]" data-nome="[nome]"
                                               class="btn text-white btn-sm btn-primary add-item">
                                                <i class="fa fa-plus fa-sm"></i> Adicionar Item
                                            </a>
                                        @endif
                                    </a>
                                </li>

                                <div class="collapse" data-parent="#accordAndamento" id="pauta[id]">
                                    <ul class="pauta-items nav list-group flex-column" data-id="[id]"></ul>
                                </div>
                            </div>
                        </div>

                        <div id="template-item" style="display:none" data-admin="{!! Auth::user()->nivel == 0 !!}">
                            <li class="pauta-item nav-item list-group-item internal-item [class]" data-id="[id]">
                                <div class="btn btn-link nav-link text-left [class]" data-toggle="collapse"
                                     data-target="#texto[id]">
                                    @if(Auth::user()->nivel == 0)
                                        <button class="btn-pauta btn btn-sm btn-[btn] [status-class]" data-id="[id]"
                                                style="display:[admin]">
                                            <i class="fa fa-fw fa-[fa]" title="[title]"></i>
                                        </button>
                                    @else
                                        <div class="indicator [status-class]" title="[title]">
                                            <i class="fa fa-fw fa-lock-open indicator-open"></i>
                                            <i class="fa fa-fw fa-lock indicator-closed"></i>
                                        </div>
                                    @endif
                                    [nome]
                                </div>
                            </li>
                        </div>
                    </ul>
                </div>
            </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-9 pt-3 px-4">

                <div class="pb-2 mb-3 border-bottom" id="quorum">
                    <h1 class="h2">Quorum</h1>
                    <ul class="quorum" id="quorum-users" data-id="{{ $reuniao->id }}">{{-- TODO: Estilizar --}}
                        @foreach($reuniao->users as $user)
                            <li data-id="{!! $user->id !!}">
                                @if($user->foto)
                                    <img src="{{ asset("arquivos/$user->foto") }}" style="max-width: 25px"
                                         class="rounded-circle">
                                @else
                                    <img src="http://siscon.amprev.ap.gov.br/img/default-avatar.png"
                                         style="max-width: 25px"
                                         class="rounded-circle">
                                @endif
                                {{ $user->name }}
                            </li>
                        @endforeach
                    </ul>
                </div>

                <div class="pb-2 mb-3 border-bottom" id="accordTexto">
                    <div id="template-texto" style="display:none">
                        <div class="item-conteudo collapse" id="texto[id]" data-parent="#accordTexto">
                            <h1 class="h2">[nome]</h1>
                            <div class="item-texto">
                                <p>[texto]</p>
                            </div>

                            <div class="voto-realizado col-md-6 col-lg-8">
                                <div class="voto-opcao voto-sim">
                                    <div class="voto-nome">Sim</div>
                                    <div class="voto-votos">Votos:
                                        <div class="voto-num">0</div>
                                    </div>
                                    <div class="voto-users"></div>
                                </div>
                                <div class="voto-opcao voto-nao">
                                    <div class="voto-nome">Não</div>
                                    <div class="voto-votos">Votos:
                                        <div class="voto-num">0</div>
                                    </div>
                                    <div class="voto-users"></div>
                                </div>
                                <div class="voto-opcao voto-abs">
                                    <div class="voto-nome">Abstenção</div>
                                    <div class="voto-votos">Votos:
                                        <div class="voto-num">0</div>
                                    </div>
                                    <div class="voto-users"></div>
                                </div>
                            </div>
                            <div class="voto-pendente col-md-6 col-lg-8">
                                <h3>Votar</h3>
                                <form action="/reunioes/votar" method="POST" class="voto-form">
                                    @csrf
                                    <input type="hidden" name="item_pauta_id" value="[id]"/>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="voto" id="voto[id]_1"
                                               value="{!! \App\Models\Voto::VOTO_SIM !!}">
                                        <label class="form-check-label" for="voto[id]_1">
                                            Sim
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="voto" id="voto[id]_2"
                                               value="{!! \App\Models\Voto::VOTO_NAO !!}">
                                        <label class="form-check-label" for="voto[id]_2">
                                            Não
                                        </label>
                                    </div>
                                    Adendo
                                    <textarea class="form-control voto-adendo" name="adendo"></textarea>
                                    <button type="submit">Votar</button>
                                </form>
                            </div>
                        </div>{{-- /.collapse --}}
                    </div>{{-- /#template-texto --}}
                </div>{{-- /#accordTexto --}}

            </main>
        </div>
    </div>


    @if(Auth::user()->nivel == 0)
        <div id="addPautaDiv" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title text-white">Incluir Pauta</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="addPautaForm" action="{{ route('pauta.store') }}" class="needs-validation" novalidate
                          method="POST" enctype="application/x-www-form-urlencoded">
                        {!! csrf_field() !!}
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Título"
                                           required autocomplete="off">
                                    <div class="valid-feedback">
                                        Ok !
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="reuniao_id" value="{{ $reuniao->id }}">
                            <button type="submit" class="btn btn-primary">Adicionar</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="addItemDiv" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title text-white">Incluir Item na Pauta: <span class="pauta-nome"></span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="addItemForm" action="{{ route('itempauta.store') }}" method="post"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <input type="text" class="form-control" id="nome" name="nome"
                                           placeholder="Descrição"
                                           required autocomplete="off">
                                </div>
                                <div class="col-md-12 mb-3">
                                    <textarea class="form-control" id="texto" name="texto" title="texto"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="pauta_id" value="" class="pauta-id">
                            <button type="submit" class="btn btn-primary">Adicionar</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
@endsection

@push('js')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/pusher.min.js') }}"></script>
    <script src="{!! asset('js/reuniao.js') !!}"></script>
    <script src="{{ asset('packages/new/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('packages/new/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        jQuery(document).ready(function($) {
            $("#texto").ckeditor({
                "filebrowserBrowseUrl": "http://localhost:8000/admin/elfinder/ckeditor",
                "extraPlugins" : 'oembed,widget'
            });
        });
    </script>
    <!-- include browse server js -->
    <script src="{{ asset('packages/new/colorbox/jquery.colorbox-min.js') }}"></script>
    <script>
        $(document).on('click','.popup_selector[data-inputid=image-filemanager]',function (event) {
            event.preventDefault();

            // trigger the reveal modal with elfinder inside
            var triggerUrl = "http://localhost:8000/admin/elfinder/popup/image-filemanager";
            $.colorbox({
                href: triggerUrl,
                fastIframe: true,
                iframe: true,
                width: '80%',
                height: '80%'
            });
        });

        // function to update the file selected by elfinder
        function processSelectedFile(filePath, requestingField) {
            $('#' + requestingField).val(filePath.replace(/\\/g,"/"));
        }

        $(document).on('click','.clear_elfinder_picker[data-inputid=image-filemanager]',function (event) {
            event.preventDefault();
            var updateID = $(this).attr('data-inputid'); // Btn id clicked
            $("#"+updateID).val("");
        });
    </script>
@endpush
