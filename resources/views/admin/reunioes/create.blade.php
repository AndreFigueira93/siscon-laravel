@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-default">
                    <div class="card-header">
                        <h5><i class="fa fa-globe"></i> Administração de Reuniões</h5>
                        <hr>
                        <a href="{{ route('reunioes.index') }}" class="btn btn-outline-info btn-sm">
                            <i class="fa fa-backward"></i> Voltar
                        </a>
                    </div>
                    <div class="card-body">
                        <form method="POST" enctype="application/x-www-form-urlencoded" novalidate action="{{ route('reunioes.store') }}">
                            @csrf
                            <div class="form-row align-items-center">
                                <div class="col-sm-12 col-md-12">
                                    <label class="sr-only" for="descricao">Descrição</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-font"></i></div>
                                        </div>
                                        <textarea class="form-control {{ $errors->has('descricao') ? ' is-invalid' : '' }}"
                                                  id="descricao"
                                                  name="descricao"
                                                  placeholder="Descrição da Reunião" required>{{ old('descricao') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-5">
                                    <label class="sr-only" for="data">Data</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                        <input type="date" class="form-control {{ $errors->has('data') ? ' is-invalid' : '' }}" id="data" name="data" placeholder="dd/mm/aaaa">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-5">
                                    <label class="sr-only" for="status">Status</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-info"></i></div>
                                        </div>
                                        <select id="status" readonly required name="status" class="form-control {{ $errors->has('status') ? ' is-invalid' : '' }}">
                                            <option value="0">Fechada</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-2">
                                    <button type="submit" class="btn btn-outline-info mb-2">
                                        <i class="fa fa-plus"></i>
                                        Cadastrar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection