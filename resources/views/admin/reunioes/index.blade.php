@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-default">
                    <div class="card-header">
                        <h5><i class="fa fa-globe"></i> Administração de Reuniões</h5>
                        <hr>
                        <a href="{{ route('reunioes.create') }}" class="btn btn-outline-info btn-sm">
                            <i class="fa fa-plus"></i> Nova Reunião
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover text-center">
                                <thead>
                                <tr>
                                    <th>N*</th>
                                    <th>Descrição</th>
                                    <th>Data</th>
                                    <th>Relação</th>
                                    <th>Administração</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($reunioes as $re)
                                    <tr>
                                        <td>{{ $re->id }}</td>
                                        <td>{{ $re->descricao }}</td>
                                        <td>{{ date('d-m-Y', strtotime($re->data)) }}</td>
                                        <td>
                                            <a href="#" class="visualizar" data-id="{{ $re->id }}">
                                                <span class="badge badge-info">
                                                    <i class="fa fa-search"></i>
                                                    Visualizar
                                                </span>
                                            </a>
                                        </td>
                                        <td>
                                            <form method="post" enctype="application/x-www-form-urlencoded"
                                                  action="{{ route('reunioes.destroy', $re->id) }}">
                                                @csrf
                                                {{ method_field('DELETE') }}
                                                <a href="{{ route('reunioes.edit', $re->id) }}" data-toggle="tooltip"
                                                   title="Editar" class="btn btn-sm btn-outline-warning">
                                                    <i class="fa fa-pencil-alt"></i>
                                                </a>
                                                <button class="btn btn-sm btn-outline-danger" data-toggle="tooltip"
                                                        title="Excluir">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true"></div>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/pace/center-radar.css') }}">
@endsection

@push('js')
    <script src="{{ asset('js/pace.js') }}"></script>
    <script>
        $('.visualizar').on('click', function () {
            let id = $(this).data('id');
            let divModal = $('#modal');
            let rota = "/historicoReuniao/" + id;

            $.ajax({
                url: rota,
                beforeSend: function () {
                    $('#app').addClass('')
                },
                error: function (err, xhr) {
                    console.log(xhr)
                },
                success: function (data) {
                    divModal.html(data);
                    divModal.modal('toggle')
                },
                complete: function () {
                    $('#app').removeClass('')
                }
            })
        });
    </script>
@endpush