@extends('layouts.app')

@section('css')
    <link href="{{ asset('packages/cropper/dist/cropper.min.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        .hide {
            display: none;
        }

        .image .btn-group {
            margin-top: 10px;
        }

        img {
            max-width: 100%; /* This rule is very important, please do not ignore this! */
        }

        .img-container, .img-preview {
            width: 100%;
            text-align: center;
        }

        .img-preview {
            float: left;
            margin-right: 10px;
            margin-bottom: 10px;
            overflow: hidden;
        }

        .preview-lg {
            width: 263px;
            height: 148px;
        }

        .btn-file {
            position: relative;
            overflow: hidden;
        }

        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }

        .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
            color: #000;
            background-color: #fff;
            border-left: 3px solid #007bff;
            border-radius: 0 !important;
            border-bottom: 1px solid #eee;
        }

        .nav-pills .nav-link, .nav-pills .show > .nav-link {
            background-color: #fff;
            border-left: 3px solid #eee;
            border-radius: 0 !important;
            border-bottom: 1px solid #eee;
            transition: 0.5s;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-default">
                    <div class="card-header bg-info">
                        <div class="row justify-content-center">
                            <div class="col-2 mb-3 d-none d-md-block d-lg-block d-xl-block d-lg-none">
                                @if($user->foto)
                                    <img src="{{ asset("arquivos/$user->foto") }}" class="rounded-circle img-fluid">
                                @else
                                    <img src="{{ asset('img/default-avatar.png') }}" class="rounded-circle img-fluid">
                                @endif
                                <br>
                            </div>
                            <div class="col-4 mb-3 d-sm-block d-md-none">
                                @if($user->foto)
                                    <img src="{{ asset("arquivos/$user->foto") }}" class="rounded-circle img-fluid">
                                @else
                                    <img src="{{ asset('img/default-avatar.png') }}" class="rounded-circle img-fluid">
                                @endif
                                <br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-center text-white">
                                <h3 style="font-weight: bold">{{ $user->name }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <nav class="nav flex-column nav-pills" id="pills-tab">
                                    <a class="nav-link active" id="perfil-tab" data-toggle="pill"
                                       href="#perfil" role="tab" aria-controls="perfil"
                                       aria-selected="true" style="font-weight: bold">
                                        <i class="fa fa-user-circle text-dark"></i>
                                        Dados Pessoais</a>
                                    <a class="nav-link" id="alterFoto-tab" data-toggle="pill" href="#alterFoto"
                                       role="tab" aria-controls="alterSenha"
                                       aria-selected="false" style="font-weight: bold">
                                        <i class="fa fa-image text-dark"></i>
                                        Foto de Perfil</a>
                                    <a class="nav-link" id="alterSenha-tab" data-toggle="pill"
                                       href="#alterSenha" role="tab" aria-controls="alterSenha"
                                       aria-selected="false" style="font-weight: bold">
                                        <i class="fa fa-lock text-dark"></i>
                                        Alterar Senha</a>
                                </nav>
                            </div>
                            <div class="col-sm-8">
                                <hr>
                                <div class="tab-content" id="pills-content" data-spy="scroll" data-target="#pills-tab"
                                     data-offset="0">
                                    <div class="tab-pane fade show active" id="perfil" role="tabpanel"
                                         aria-labelledby="v-pills-home-tab">
                                        <form method="POST" class="validar" novalidate
                                              enctype="application/x-www-form-urlencoded"
                                              action="{{ route('perfilUpdate') }}">
                                            @csrf

                                            {{ method_field('PUT') }}
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="form-row align-items-center">
                                                        <div class="col-sm-12">
                                                            <label class="sr-only" for="name">Nome</label>
                                                            <div class="input-group mb-2">
                                                                <div class="input-group-prepend">
                                                                    <div class="input-group-text"><i
                                                                                class="fa fa-user"></i>
                                                                    </div>
                                                                </div>
                                                                <input type="text"
                                                                       class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                                       id="name" name="name"
                                                                       placeholder="Nome do Usuário"
                                                                       value="{{ $user->name }}" required>
                                                                <div class="invalid-feedback">
                                                                    Por favor, verifique este campo
                                                                </div>
                                                                <div class="valid-feedback">
                                                                    <i class="fa fa-check-circle"></i> Ok
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <label class="sr-only" for="name">Usuário</label>
                                                            <div class="input-group mb-2">
                                                                <div class="input-group-prepend">
                                                                    <div class="input-group-text"><i
                                                                                class="fa fa-street-view"></i></div>
                                                                </div>
                                                                <input type="text"
                                                                       class="form-control {{ $errors->has('usuario') ? ' is-invalid' : '' }}"
                                                                       id="usuario" name="usuario" placeholder="Usuário"
                                                                       value="{{ $user->usuario }}" required>
                                                                <div class="invalid-feedback">
                                                                    Por favor, verifique este campo
                                                                </div>
                                                                <div class="valid-feedback">
                                                                    <i class="fa fa-check-circle"></i> Ok
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <label class="sr-only" for="email">E-mail</label>
                                                            <div class="input-group mb-2">
                                                                <div class="input-group-prepend">
                                                                    <div class="input-group-text"><i
                                                                                class="fa fa-envelope"></i>
                                                                    </div>
                                                                </div>
                                                                <input type="email"
                                                                       class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                                       id="email" name="email" placeholder="E-mail"
                                                                       value="{{ $user->email }}" required>
                                                                <div class="invalid-feedback">
                                                                    Por favor, verifique este campo
                                                                </div>
                                                                <div class="valid-feedback">
                                                                    <i class="fa fa-check-circle"></i> Ok
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <label class="sr-only" for="telefone">Telefone</label>
                                                            <div class="input-group mb-2">
                                                                <div class="input-group-prepend">
                                                                    <div class="input-group-text"><i
                                                                                class="fa fa-phone"></i>
                                                                    </div>
                                                                </div>
                                                                <input type="text"
                                                                       class="form-control {{ $errors->has('telefone') ? ' is-invalid' : '' }}"
                                                                       id="telefone" name="telefone"
                                                                       placeholder="Telefone"
                                                                       value="{{ $user->telefone }}">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <label class="sr-only" for="entidade">Entidade</label>
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <div class="input-group-text"><i
                                                                                class="fa fa-university"></i></div>
                                                                </div>
                                                                <select disabled class="form-control">
                                                                    <option>{{ $user->orgao->nome }}</option>
                                                                </select>
                                                                <div class="valid-feedback">
                                                                    <i class="fa fa-check-circle"></i> Ok
                                                                </div>
                                                            </div>
                                                            <small id="orgaoHelpBlock"
                                                                   class="form-text text-muted mb-4">
                                                                <i class="fa fa-info"></i> Somente um Administrador pode
                                                                alterar a entidade vinculada
                                                            </small>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <button type="submit"
                                                                    class="btn btn-outline-info btn-block mb-2">
                                                                <i class="fa fa-share"></i>
                                                                Atualizar
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="alterSenha" role="tabpanel"
                                         aria-labelledby="alterSenha-tab">
                                        <form method="post" enctype="application/x-www-form-urlencoded"
                                              action="{{ route('resetSenha') }}">
                                            @csrf
                                            {{ method_field('PUT') }}
                                            <div class="row">
                                                <div class="col-md-6 col-sm-12">
                                                    <label class="sr-only" for="password">Senha Atual</label>
                                                    <div class="input-group mb-2">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text"><i
                                                                        class="fa fa-lock-open"></i></div>
                                                        </div>
                                                        <input type="password"
                                                               class="form-control {{ $errors->has('atual') ? ' is-invalid' : '' }}"
                                                               id="atual" name="atual" placeholder="Senha Atual"
                                                               value="{{ old('atual') }}"
                                                               required>
                                                        <div class="invalid-feedback">
                                                            Por favor, verifique este campo
                                                        </div>
                                                        <div class="valid-feedback">
                                                            <i class="fa fa-check-circle"></i> Ok
                                                        </div>
                                                    </div>
                                                    <label class="sr-only" for="password">Nova Senha</label>
                                                    <div class="input-group mb-2">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text"><i class="fa fa-key"></i>
                                                            </div>
                                                        </div>
                                                        <input type="password"
                                                               class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                               id="password" name="password" placeholder="Nova Senha"
                                                               value="{{ old('password') }}"
                                                               required>
                                                        <div class="invalid-feedback">
                                                            Por favor, verifique este campo
                                                        </div>
                                                        <div class="valid-feedback">
                                                            <i class="fa fa-check-circle"></i> Ok
                                                        </div>
                                                    </div>
                                                    <label class="sr-only" for="password">Confirme a Nova Senha</label>
                                                    <div class="input-group mb-2">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text"><i class="fa fa-key"></i>
                                                            </div>
                                                        </div>
                                                        <input type="password"
                                                               class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
                                                               id="password_confirmation" name="password_confirmation"
                                                               placeholder="Confirme a Nova Senha"
                                                               value="{{ old('password_confirmation') }}"
                                                               required>
                                                        <div class="invalid-feedback">
                                                            Por favor, verifique este campo
                                                        </div>
                                                        <div class="valid-feedback">
                                                            <i class="fa fa-check-circle"></i> Ok
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-outline-info btn-block mb-2">
                                                        <i class="fa fa-share"></i>
                                                        Salvar
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="alterFoto" role="tabpanel"
                                         aria-labelledby="alterFoto-tab">
                                        <form method="POST" enctype="multipart/form-data" action="{{ route('alterFoto') }}">
                                            @csrf
                                            @method('PUT')
                                            <div data-preview="#foto" data-aspectratio="0" data-crop="1" class="form-group col-sm-12 image">
                                                <div class="row">
                                                    <div class="col-sm-6" style="margin-bottom: 20px;">
                                                        @if($user->foto)
                                                            <img id="mainImage" src="{{ asset("arquivos/$user->foto") }}">
                                                        @else
                                                            <img id="mainImage" src="{{ asset('img/default-avatar.png') }}">
                                                        @endif
                                                    </div>
                                                    <div class="col-sm-3 text-center">
                                                        <div class="card" style="width: 18rem;">
                                                            <div class="docs-preview clearfix">
                                                                <div id="foto" class="img-preview preview-lg">
                                                                    <center>
                                                                        <img src="" style="display: block; min-width: 0px !important; min-height: 0px !important; max-width: none !important; max-height: none !important; margin-left: -32.875px; margin-top: -18.4922px; transform: none;" class="card-img-top img-fluid">
                                                                    </center>
                                                                </div>
                                                            </div>
                                                            <div class="card-body">
                                                                <h5 class="card-title">Resultado</h5>
                                                                <p class="card-text">
                                                                    <button type="submit" class="btn btn-block btn-outline-info ">
                                                                        <i class="fa fa-share"></i>
                                                                        Salvar
                                                                    </button>
                                                                </p>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="btn-group" role="group" aria-label="btn-group">
                                                    <label class="btn btn-primary btn-file" style="margin-bottom: 0;">
                                                        Escolher Imagem <input type="file" accept="image/*" id="uploadImage" class="hide">
                                                        <input type="hidden" name="foto" id="hiddenImage">
                                                    </label>
                                                    <button class="btn btn-default" id="rotateLeft" type="button" style="display: none"><i class="fa fa-chevron-left"></i></button>
                                                    <button class="btn btn-default" id="rotateRight" type="button" style="display: none;"><i class="fa fa-chevron-right"></i></button>
                                                    <button class="btn btn-default" id="zoomIn" type="button" style="display: none;"><i class="fa fa-search-plus"></i></button>
                                                    <button class="btn btn-default" id="zoomOut" type="button" style="display: none;"><i class="fa fa-search-minus"></i></button>
                                                    <button class="btn btn-warning" id="reset" type="button" style="display: none;"><i class="fa fa-times"></i></button>
                                                    <button class="btn btn-danger" id="remove" type="button"><i class="fa fa-trash"></i></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function () {
            'use strict';
            window.addEventListener('load', function () {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('validar');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
    <script>
        var pass = $('#password');
        var passcon = $('#password_confirmation');

        function length(e) {
            return e.val().length >= 6
        }

        function equal() {
            return pass.html === passcon.html
        }

        pass.on('keyup', function () {
            if (length(pass) === true) {
                pass.removeClass('is-invalid');
                pass.addClass('is-valid')
            } else {
                pass.removeClass('is-valid');
                pass.addClass('is-invalid')
            }
        });

        passcon.on('keyup', function () {
            if (length(passcon) && equal() === true) {
                passcon.removeClass('is-invalid');
                passcon.addClass('is-valid')
            } else {
                passcon.removeClass('is-valid');
                passcon.addClass('is-invalid');
            }
        });
    </script>

    <script src="{{ asset('packages/cropper/dist/cropper.min.js') }}"></script>
    <script>
        jQuery(document).ready(function ($) {
            // Loop through all instances of the image field
            $('.form-group.image').each(function (index) {
                // Find DOM elements under this form-group element
                var $mainImage = $(this).find('#mainImage');
                var $uploadImage = $(this).find("#uploadImage");
                var $hiddenImage = $(this).find("#hiddenImage");
                var $rotateLeft = $(this).find("#rotateLeft");
                var $rotateRight = $(this).find("#rotateRight");
                var $zoomIn = $(this).find("#zoomIn");
                var $zoomOut = $(this).find("#zoomOut");
                var $reset = $(this).find("#reset");
                var $remove = $(this).find("#remove");
                // Options either global for all image type fields, or use 'data-*' elements for options passed in via the CRUD controller
                var options = {
                    viewMode: 2,
                    checkOrientation: false,
                    autoCropArea: 1,
                    responsive: true,
                    preview: $(this).attr('data-preview'),
                    aspectRatio: $(this).attr('data-aspectRatio')
                };
                var crop = $(this).attr('data-crop');

                // Hide 'Remove' button if there is no image saved
                if (!$mainImage.attr('src')) {
                    $remove.hide();
                }
                // Initialise hidden form input in case we submit with no change
                $hiddenImage.val($mainImage.attr('src'));


                // Only initialize cropper plugin if crop is set to true
                if (crop) {

                    $remove.click(function () {
                        $mainImage.cropper("destroy");
                        $mainImage.attr('src', '');
                        $hiddenImage.val('');
                        $rotateLeft.hide();
                        $rotateRight.hide();
                        $zoomIn.hide();
                        $zoomOut.hide();
                        $reset.hide();
                        $remove.hide();
                    });
                } else {

                    $(this).find("#remove").click(function () {
                        $mainImage.attr('src', '');
                        $hiddenImage.val('');
                        $remove.hide();
                    });
                }

                $uploadImage.change(function () {
                    var fileReader = new FileReader(),
                        files = this.files,
                        file;

                    if (!files.length) {
                        return;
                    }
                    file = files[0];

                    if (/^image\/\w+$/.test(file.type)) {
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function () {
                            $uploadImage.val("");
                            if (crop) {
                                $mainImage.cropper(options).cropper("reset", true).cropper("replace", this.result);
                                // Override form submit to copy canvas to hidden input before submitting
                                $('form').submit(function () {
                                    var imageURL = $mainImage.cropper('getCroppedCanvas').toDataURL(file.type);
                                    $hiddenImage.val(imageURL);
                                    return true; // return false to cancel form action
                                });
                                $rotateLeft.click(function () {
                                    $mainImage.cropper("rotate", 90);
                                });
                                $rotateRight.click(function () {
                                    $mainImage.cropper("rotate", -90);
                                });
                                $zoomIn.click(function () {
                                    $mainImage.cropper("zoom", 0.1);
                                });
                                $zoomOut.click(function () {
                                    $mainImage.cropper("zoom", -0.1);
                                });
                                $reset.click(function () {
                                    $mainImage.cropper("reset");
                                });
                                $rotateLeft.show();
                                $rotateRight.show();
                                $zoomIn.show();
                                $zoomOut.show();
                                $reset.show();
                                $remove.show();

                            } else {
                                $mainImage.attr('src', this.result);
                                $hiddenImage.val(this.result);
                                $remove.show();
                            }
                        };
                    } else {
                        alert("Por favor, selecione uma IMAGEM");
                    }
                });

            });
        });
    </script>
@endpush