@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card card-default">
                    <div class="card-header">
                        <h5><i class="fa fa-street-view"></i>
                             Administração de Usuários</h5>
                        <hr>
                        <a href="{{ route('usuarios.index') }}" class="btn btn-outline-primary btn-sm">
                            <i class="fa fa-backward"></i> Voltar
                        </a>
                        <a href="{{ route('usuarios.create') }}" class="btn btn-outline-info btn-sm">
                            <i class="fa fa-plus"></i> Novo Usuário
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover text-center">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>E-mail</th>
                                    <th>Telefone</th>
                                    <th>Orgão</th>
                                    <th>Ativo</th>
                                    <th>Nível</th>
                                    <th>Histórico</th>
                                    <th>Administração</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($usuarios as $usr)
                                    <tr>
                                        <td>{{ $usr->name }}</td>
                                        <td>{{ $usr->email }}</td>
                                        <td>{{ $usr->telefone }}</td>
                                        <td>{{ $usr->orgao->nome }}</td>
                                        <td>
                                            @if ($usr->ativo == 1)
                                                <i class="fa fa-check" style="color: green"></i>
                                            @else
                                                <i class="fa fa-ban" style="color: red"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($usr->nivel == 0)
                                                <span class="badge badge-dark">
                                                <i class="fa fa-user-secret"></i> Administrador
                                                </span>
                                            @else
                                                <span class="badge badge-secondary">
                                                <i class="fa fa-user"></i> Comum
                                                </span>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="#" class="visualizar" data-id="{{ $usr->id }}">
                                                <span class="badge badge-info">
                                                    <i class="fa fa-search"></i>
                                                    Visualizar
                                                </span>
                                            </a>
                                        </td>
                                        <td>
                                            <form method="post" style="display: inline" enctype="application/x-www-form-urlencoded"
                                                  action="{{ route('restore-user', $usr->id) }}">
                                                @csrf
                                                <button class="btn btn-sm btn-outline-primary" data-toggle="tooltip"
                                                        title="Restaurar">
                                                    <i class="fa fa-history"></i>
                                                </button>
                                            </form>
                                            <form method="post" style="display: inline" enctype="application/x-www-form-urlencoded"
                                                  action="{{ route('delete-user', $usr->id) }}">
                                                @csrf
                                                <button class="btn btn-sm btn-outline-danger" data-toggle="tooltip"
                                                        title="Excluir">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true"></div>
@endsection

@push('js')
    <script>
        $('.visualizar').on('click', function () {
            let id = $(this).data("id");
            let rota = "/admin/usuarios/" + id;
            let divModal = $('#modal');
            $.ajax({
                url: rota,
                success: function (data) {
                    divModal.html(data);
                    divModal.modal({
                        backdrop: false,
                        show: true
                    }).toggle()
                }
            });
        });
    </script>
@endpush