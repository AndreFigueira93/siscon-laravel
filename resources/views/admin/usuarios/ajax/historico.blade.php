<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header bg-info">
            <h3 class="modal-title text-white"><b>Usuário: </b>{{ $user->name }}</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table class="table table-bordered table-bordered">
                    <thead>
                    <tr>
                        <th>Data</th>
                        <th>Reunião</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($info as $inf)
                        <tr class="text-center @if($inf['status'] == 'Presente') table-success @else table-danger @endif">
                            <td>{{ $inf['data'] }}</td>
                            <td>{{ $inf['reuniao'] }}</td>
                            <td>{!! $inf['icon'] !!} {{ $inf['status'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
    </div>
</div>