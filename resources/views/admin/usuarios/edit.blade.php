@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card card-default">
                    <div class="card-header">
                        <h5>Administração de Usuários</h5>
                        <hr>
                        <a href="{{ route('usuarios.index') }}" class="btn btn-outline-info btn-sm">
                            <i class="fa fa-backward"></i> Voltar
                        </a>
                        <sub style="color: red">
                            <i class="fa fa-exclamation-circle"></i>
                            Para alterar a senha, o próprio usuário deve realizar o processo de recuperação
                        </sub>
                    </div>
                    <div class="card-body">
                        <form method="POST" class="validar" novalidate enctype="application/x-www-form-urlencoded"
                              action="{{ route('usuarios.update', $user->id) }}">
                            @csrf

                            {{ method_field('PUT') }}

                            <div class="form-row align-items-center">
                                <div class="col-sm-12">
                                    <label class="sr-only" for="name">Nome</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-user"></i></div>
                                        </div>
                                        <input type="text"
                                               class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                                               id="name" name="name" placeholder="Nome do Usuário"
                                               value="{{ $user->name }}" required>
                                        <div class="invalid-feedback">
                                            Por favor, verifique este campo
                                        </div>
                                        <div class="valid-feedback">
                                            <i class="fa fa-check-circle"></i> Ok
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label class="sr-only" for="name">Usuário</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-street-view"></i></div>
                                        </div>
                                        <input type="text"
                                               class="form-control {{ $errors->has('usuario') ? ' is-invalid' : '' }}"
                                               id="usuario" name="usuario" placeholder="Usuário"
                                               value="{{ $user->usuario }}" required>
                                        <div class="invalid-feedback">
                                            Por favor, verifique este campo
                                        </div>
                                        <div class="valid-feedback">
                                            <i class="fa fa-check-circle"></i> Ok
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label class="sr-only" for="email">E-mail</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-envelope"></i></div>
                                        </div>
                                        <input type="email"
                                               class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                               id="email" name="email" placeholder="E-mail"
                                               value="{{ $user->email }}" required>
                                        <div class="invalid-feedback">
                                            Por favor, verifique este campo
                                        </div>
                                        <div class="valid-feedback">
                                            <i class="fa fa-check-circle"></i> Ok
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label class="sr-only" for="telefone">Telefone</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-phone"></i></div>
                                        </div>
                                        <input type="tel"
                                               class="form-control {{ $errors->has('telefone') ? ' is-invalid' : '' }}"
                                               id="telefone" name="telefone" placeholder="Telefone"
                                               value="{{ $user->telefone }}">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4">
                                    <label class="sr-only" for="orgao">Orgão</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-university"></i></div>
                                        </div>
                                        <select class="form-control {{ $errors->has('orgao_id') ? ' is-invalid' : '' }}"
                                                id="orgao" name="orgao_id" required>
                                            @foreach($orgaos as $org)
                                                <option value="{{ $org->id }}"
                                                        @if($org->id == $user->orgao_id) selected @endif>{{ $org->nome }}</option>
                                            @endforeach
                                        </select>
                                        <div class="valid-feedback">
                                            <i class="fa fa-check-circle"></i> Ok
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4">
                                    <label class="sr-only" for="ativo">Ativo</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-globe"></i></div>
                                        </div>
                                        <select class="form-control {{ $errors->has('ativo') ? ' is-invalid' : '' }}"
                                                id="ativo" name="ativo" required>
                                            <option value="1" @if($user->ativo == 1) selected @endif>Ativo</option>
                                            <option value="0" @if($user->ativo == 0) selected @endif>Inativo</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Por favor, verifique este campo
                                        </div>
                                        <div class="valid-feedback">
                                            <i class="fa fa-check-circle"></i> Ok
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4">
                                    <label class="sr-only" for="nivel">Nível</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-gavel"></i></div>
                                        </div>
                                        <select class="form-control {{ $errors->has('nivel') ? ' is-invalid' : '' }}"
                                                id="nivel" name="nivel" required>
                                            <option value="1" @if($user->nivel == 1) selected @endif>Comum</option>
                                            <option value="0" @if($user->nivel == 0) selected @endif>Administrador</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Por favor, verifique este campo
                                        </div>
                                        <div class="valid-feedback">
                                            <i class="fa fa-check-circle"></i> Ok
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-outline-info btn-block mb-2">
                                        <i class="fa fa-share"></i>
                                        Atualizar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function () {
            'use strict';
            window.addEventListener('load', function () {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('validar');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
@endpush