@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-default">
                    <div class="card-header">
                        <h5><i class="fa fa-street-view"></i> Administração de Usuários</h5>
                        <hr>
                        <a href="{{ route('usuarios.index') }}" class="btn btn-outline-info btn-sm">
                            <i class="fa fa-backward"></i> Voltar
                        </a>
                    </div>
                    <div class="card-body">
                        <form method="POST" class="validar" novalidate enctype="application/x-www-form-urlencoded"
                              action="{{ route('usuarios.store') }}">
                            @csrf
                            <div class="form-row align-items-center">
                                <div class="col-sm-12">
                                    <label class="sr-only" for="name">Nome</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-user"></i></div>
                                        </div>
                                        <input type="text"
                                               class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                                               id="name" name="name" placeholder="Nome do Usuário"
                                               value="{{ old('name') }}"
                                               required>
                                        <div class="invalid-feedback">
                                            Por favor, verifique este campo
                                        </div>
                                        <div class="valid-feedback">
                                            <i class="fa fa-check-circle"></i> Ok
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label class="sr-only" for="name">Usuário</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-street-view"></i></div>
                                        </div>
                                        <input type="text"
                                               class="form-control {{ $errors->has('usuario') ? ' is-invalid' : '' }}"
                                               id="usuario" name="usuario" placeholder="Usuário"
                                               value="{{ old('usuario') }}"
                                               required>
                                        <div class="invalid-feedback">
                                            Por favor, verifique este campo
                                        </div>
                                        <div class="valid-feedback">
                                            <i class="fa fa-check-circle"></i> Ok
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label class="sr-only" for="email">E-mail</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-envelope"></i></div>
                                        </div>
                                        <input type="email"
                                               class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                               id="email" name="email" placeholder="E-mail" value="{{ old('email') }}"
                                               required>
                                        <div class="invalid-feedback">
                                            Por favor, verifique este campo
                                        </div>
                                        <div class="valid-feedback">
                                            <i class="fa fa-check-circle"></i> Ok
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label class="sr-only" for="password">Senha</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-key"></i></div>
                                        </div>
                                        <input type="password"
                                               class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                               id="password" name="password" placeholder="Senha"
                                               value="{{ old('password') }}"
                                               required>
                                        <div class="invalid-feedback">
                                            Por favor, verifique este campo
                                        </div>
                                        <div class="valid-feedback">
                                            <i class="fa fa-check-circle"></i> Ok
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label class="sr-only" for="password_confirmation">Confirme a Senha</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-key"></i></div>
                                        </div>
                                        <input type="password"
                                               class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
                                               id="password_confirmation" name="password_confirmation"
                                               placeholder="Confirme a Senha" value="{{ old('password_confirmation') }}"
                                               required>
                                        <div class="invalid-feedback">
                                            Por favor, verifique este campo
                                        </div>
                                        <div class="valid-feedback">
                                            <i class="fa fa-check-circle"></i> Ok
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label class="sr-only" for="telefone">Telefone</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-phone"></i></div>
                                        </div>
                                        <input type="tel"
                                               class="form-control {{ $errors->has('telefone') ? ' is-invalid' : '' }}"
                                               id="telefone" name="telefone" placeholder="Telefone"
                                               value="{{ old('telefone') }}"
                                        >
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4">
                                    <label class="sr-only" for="orgao">Orgão</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-university"></i></div>
                                        </div>
                                        <select class="form-control {{ $errors->has('orgao_id') ? ' is-invalid' : '' }}"
                                                id="orgao" name="orgao_id" required>
                                            @foreach($orgaos as $org)
                                                <option value="{{ $org->id }}" {{ (old("orgao_id") == $org->id ? "selected":"") }}>{{ $org->nome }}</option>
                                            @endforeach
                                        </select>
                                        <div class="valid-feedback">
                                            <i class="fa fa-check-circle"></i> Ok
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4">
                                    <label class="sr-only" for="ativo">Ativo</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-globe"></i></div>
                                        </div>
                                        <select class="form-control {{ $errors->has('ativo') ? ' is-invalid' : '' }}"
                                                id="ativo" name="ativo" required>
                                            <option value="1" @if(old('ativo') == 1) selected @endif>Ativo</option>
                                            <option value="0" @if(old('ativo') == 0) selected @endif>Inativo</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Por favor, verifique este campo
                                        </div>
                                        <div class="valid-feedback">
                                            <i class="fa fa-check-circle"></i> Ok
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4">
                                    <label class="sr-only" for="nivel">Nível</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-gavel"></i></div>
                                        </div>
                                        <select class="form-control {{ $errors->has('nivel') ? ' is-invalid' : '' }}"
                                                id="nivel" name="nivel" required>
                                            <option value="1" @if(old('nivel') == 1) selected @endif>Comum</option>
                                            <option value="0" @if(old('nivel') == 0) selected @endif>Administrador
                                            </option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Por favor, verifique este campo
                                        </div>
                                        <div class="valid-feedback">
                                            <i class="fa fa-check-circle"></i> Ok
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-outline-info btn-block mb-2">
                                        <i class="fa fa-plus"></i>
                                        Criar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
<script>
    var pass = $('#password');
    var passcon = $('#password_confirmation');

    function length(e) {
        return e.val().length >= 6
    }

    function equal() {
        return pass.html === passcon.html
    }

    pass.on('keyup', function() {
        if (length(pass) === true){
            pass.removeClass('is-invalid');
            pass.addClass('is-valid')
        } else {
            pass.removeClass('is-valid');
            pass.addClass('is-invalid')
        }
    });

    passcon.on('keyup', function() {
        if (length(passcon) && equal() === true){
            passcon.removeClass('is-invalid');
            passcon.addClass('is-valid')
        } else {
            passcon.removeClass('is-valid');
            passcon.addClass('is-invalid');
        }
    });
</script>
@endpush