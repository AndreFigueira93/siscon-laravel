<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Distribuição de Processo</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <center>
                @isset($let)

                    @if($let->foto)
                        <img class="rounded-circle" style="max-width: 300px" src="arquivos/{{ $let->foto }}">
                    @else
                        <img class="rounded-circle" style="max-width: 300px" src="/img/default-avatar.png">
                    @endif

                    <h1>{{ $let->name }}</h1>
                @endisset
            </center>
        </div>
        <div class="modal-footer">
            <button type="button" id="startDist" class="btn btn-primary">
                Sortear
            </button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">
                <b>Fechar Janela</b>
            </button>
        </div>
    </div>
</div>

<script>
    $('#startDist').on('click', function () {
        let divModal = $('#distModal');
        let rota = "/admin/distribuicao/create";

        $.ajax({
            url: rota,
            error: function (err, xhr) {
                console.log(xhr)
            },
            success: function (data) {
                divModal.html(data)
            }
        })
    });
</script>