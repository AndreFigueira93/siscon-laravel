@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-default">
                    <div class="card-header">
                        <h5><i class="fa fa-university"></i> Administração de Entidades</h5>
                        <hr>
                        <a href="{{ route('entidades.index') }}" class="btn btn-outline-info btn-sm">
                            <i class="fa fa-backward"></i> Voltar
                        </a>
                    </div>
                    <div class="card-body">
                        <form method="POST" enctype="application/x-www-form-urlencoded" action="{{ route('entidades.store') }}">
                            @csrf
                            <div class="form-row align-items-center">
                                <div class="col-sm-12 col-md-10">
                                    <label class="sr-only" for="orgao">Nome</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-university"></i></div>
                                        </div>
                                        <input type="text" class="form-control" id="orgao" name="nome" placeholder="Nome da Entidade">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-2">
                                    <button type="submit" class="btn btn-outline-info mb-2">
                                        <i class="fa fa-plus"></i>
                                        Cadastrar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection