@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-default">
                    <div class="card-header">
                        <h5><i class="fa fa-university"></i>
                            Administração de Entidades</h5>
                        <hr>
                        <a href="{{ route('entidades.index') }}" class="btn btn-outline-info btn-sm">
                            <i class="fa fa-backward"></i> Voltar
                        </a>
                    </div>
                    <div class="card-body">
                        <form method="POST" class="validar" novalidate enctype="application/x-www-form-urlencoded"
                              action="{{ route('entidades.update', $orgao->id) }}">
                            @csrf

                            {{ method_field('PUT') }}

                            <div class="form-row align-items-center">
                                <div class="col-sm-12 col-md-10">
                                    <label class="sr-only" for="orgao">Nome</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-university"></i></div>
                                        </div>
                                        <input type="text"
                                               class="form-control {{ $errors->has('nome') ? ' is-invalid' : '' }}"
                                               id="orgao" name="nome" placeholder="Nome do Entidade"
                                               value="{{ $orgao->nome }}" required>
                                        <div class="invalid-feedback">
                                            Por favor, digite o nome do Entidade
                                        </div>
                                        <div class="valid-feedback">
                                            <i class="fa fa-check-circle"></i> Ok
                                        </div>
                                        @if ($errors->has('nome'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('nome') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-2">
                                    <button type="submit" class="btn btn-outline-info mb-2">
                                        <i class="fa fa-share"></i>
                                        Atualizar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function () {
            'use strict';
            window.addEventListener('load', function () {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('validar');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
@endpush