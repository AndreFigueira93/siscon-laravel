@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-default">
                    <div class="card-header">
                        <h5><i class="fa fa-university"></i> Administração de Entidades</h5>
                        <hr>
                        <a href="{{ route('entidades.create') }}" class="btn btn-outline-info btn-sm">
                            <i class="fa fa-plus"></i> Nova Entidade
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover text-center">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Membros</th>
                                    <th>Administração</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orgaos as $org)
                                    <tr>
                                        <td>{{ $org->nome }}</td>
                                        <td>
                                            <button type="button" class="visualizar btn btn-outline-info btn-sm"
                                                    data-id="{{ $org->id }}">
                                                Visualizar <span
                                                        class="badge badge-light">{{ $org->usuarios->count() }}</span>
                                            </button>
                                        </td>
                                        <td>
                                            <form method="post" enctype="application/x-www-form-urlencoded" action="{{ route('entidades.destroy', $org->id) }}">
                                                @csrf
                                                {{ method_field('DELETE') }}
                                                <a href="{{ route('entidades.edit', $org->id) }}" data-toggle="tooltip"
                                                   title="Editar" class="btn btn-sm btn-outline-warning">
                                                    <i class="fa fa-pencil-alt"></i>
                                                </a>
                                                <button class="btn btn-sm btn-outline-danger" data-toggle="tooltip" title="Excluir">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true"></div>
@endsection

@push('js')
    <script>
        $('.visualizar').on('click', function () {
            let id = $(this).data("id");
            let rota = "/admin/entidades/" + id;
            let divModal = $('#modal');
            $.ajax({
                url: rota,
                success: function (data) {
                    divModal.html(data);
                    divModal.modal({
                        backdrop: false,
                        show: true
                    }).toggle()
                }
            });
        });
    </script>
@endpush