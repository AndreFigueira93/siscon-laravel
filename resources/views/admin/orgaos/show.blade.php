<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header dark">
            <h4>Membros</h4>
            <button type="button" class="fechar close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            @if($membros->count() > 0)
                <table class="table table-bordered">
                    @foreach($membros as $m)
                        <tr>
                            <td>
                        <span class="teste">
                    @if($m->foto)
                                <img src="{{ asset("img/$m->foto") }}" class="ui avatar image">
                            @else
                                <img src="{{ asset('img/default-avatar.png') }}" class="ui avatar image">
                            @endif
                            {{ $m->name }}</span>
                            </td>
                        </tr>
                    @endforeach
                </table>
            @else
                <div class="text-center">
                    <span class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> Não há membros registrados nesta Entidade</span>
                </div>
            @endif
        </div>
        <div class="modal-footer">
            <button type="button" class="fechar btn btn-outline-secondary">Fechar</button>
        </div>
    </div>
</div>

<script>
    $('.fechar').on('click', function () {
        $('#modal').modal().toggle()
    })
</script>