<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Comuns
Auth::routes();
Route::get('/', 'HomeController@index')->name('inicio');
Route::get('/home', 'HomeController@index')->name('home');

//Block de Registros externos
Route::any('/register', function () {
    return redirect()->route('inicio');
});

//Rotas Administrativas
Route::group(['middleware' => 'auth', 'prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::resource('distribuicao', 'DistribuicaoCrudController');
    Route::resource('reunioes', 'ReuniaoCrudController');
    Route::resource('usuarios', 'UsuarioCrudController');
    Route::resource('entidades', 'OrgaoCrudController');
    Route::resource('pauta', 'PautaCrudController');
    Route::resource('itempauta', 'ItemPautaCrudController');
    Route::get('usuarios-removidos', 'UsuarioCrudController@removidos')->name('removidos');
    Route::post('perma-delete/{id}', 'UsuarioCrudController@forceDelete')->name('delete-user');
    Route::post('restaurar-usuario/{id}', 'UsuarioCrudController@restore')->name('restore-user');
    Route::get('arquivos', 'NovoElfinderController@showIndex')->name('arquivos');
});

//Rota que Altera o Gerenciador de Arquivos
Route::group(array('middleware'=>'auth', 'namespace' => 'Admin'),function(){
    Route::any('connector', ['as' => 'elfinder.connector', 'uses' => 'NovoElfinderController@showConnectorext']);
});

Route::group(['middleware' => 'auth'], function () {
    // Rotas do Perfil
    Route::get('perfil', 'Admin\PerfilController@perfil')->name('perfil');
    Route::put('perfil', 'Admin\PerfilController@perfilUpdate')->name('perfilUpdate');
    Route::put('resetSenha', 'Admin\PerfilController@resetSenha')->name('resetSenha');

    // Rotas da Imagem do Perfil
    Route::put('alterFoto', 'Admin\PerfilController@alterFoto')->name('alterFoto');

    // Rotas das Reuniões - Comuns
    Route::get('reunioes', 'ReuniaoController@index');
    Route::get('reunioes/participar/{reuniao}', 'ReuniaoController@participar')->name('participar');
    Route::post('reunioes/votar', 'ReuniaoController@votar');
    Route::post('reunioes/{reuniao}/abrir', 'ReuniaoController@abrir');
    Route::post('reunioes/{reuniao}/encerrar', 'ReuniaoController@encerrar');
    Route::post('reunioes/{reuniao}/checar', 'ReuniaoController@checar');

    // JSON
    Route::get('reuniao/{reuniao}/load', 'ReuniaoController@loadJSON');
    Route::get('item/{item}/votos', 'ReuniaoController@votosJSON');
    Route::get ('quorum/count/{id}',    'ReuniaoController@quorumCount');
    Route::get ('quorum/users/{id}',    'ReuniaoController@quorumUsers');

    // Rotas de Respostas AJAX
    Route::get('getPautas/{id}', 'AjaxRequestsController@getPautas')->name('getPautas');
    Route::get('getUsers/{id}', 'AjaxRequestsController@getUsers')->name('getQuorum');
    Route::get('getReunions', 'AjaxRequestsController@getReunions')->name('getReunions');
    Route::any('delPauta/{id}', 'AjaxRequestsController@delPauta')->name('delPauta');
    Route::get('addItemView/{id}', 'AjaxRequestsController@addItemView')->name('addItem');
    Route::post('liberarItem/{id}', 'AjaxRequestsController@liberarItem')->name('liberarItem');
    Route::post('encerrarItem/{id}', 'AjaxRequestsController@encerrarItem')->name('encerrarItem');
    Route::get('historicoUser/{user}', 'AjaxRequestsController@historicoUser')->name('historicoUser');
    Route::get('historicoReuniao/{reuniao}', 'AjaxRequestsController@historicoReuniao')->name('historicoReuniao');
});
