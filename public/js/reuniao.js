var reuniaoID = $('#reuniao-id').data('id');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

//:: Enfileiramento de ações, visa garantir a sincronia e a ordem do que acontece :://

var lastPromise = Promise.resolve();

function enfileirar(acao, isPromise) {
    lastPromise.then(() => {
        lastPromise = isPromise ? acao : new Promise((resolve, reject) => {
            try {
                acao();
                resolve();
            } catch (e) {
                reject(e);
            }
        });
    })
    .catch(err => {
        location.reload();
    });
}

//:: Carregamento Inicial :://

enfileirar(() => carregarReuniao());

function carregarReuniao() {
    $.getJSON('/reuniao/' + reuniaoID + '/load', function(reuniao) {
        reuniao.pautas.forEach(pauta => {
            adicionarPauta(pauta);

            pauta.items.forEach(item => {
                item['reuniao_id'] = reuniao.id;
                adicionarItem(item);
                enfileirar(() => carregarVotos(item));
            });
        });
    });
}

function carregarVotos(item) {
    $.getJSON('/item/' + item.id + '/votos', function(votos) {
        votos.forEach(adicionarVoto);
    });
}

//:: Pusher :://

var pusher = new Pusher('535e10e6506389d4ef3c', {
    cluster: 'us2',
    encrypted: false
});

var channel = pusher.subscribe('reuniao-' + reuniaoID);

channel.bind('pauta.store', adicionarPauta);
channel.bind('item.store', adicionarItem);
channel.bind('item.update', atualizarItem);
channel.bind('voto.store', adicionarVoto);
channel.bind('reuniao.status', atualizarStatus);
channel.bind('user.join', atualizarQuorum);

//:: Manipulação do DOM :://

function buildarTemplate(html, data) {
    for (val in data) {
        html = html.replace(new RegExp('\\[' + val + '\\]', 'g'), data[val]);
    }

    var template = $(html);
    return template;
}

// ---- user.join ----//
var quorumCount = 0;
var quorumId = null;
var quorumDiv = $('#quorum-users');

function atualizarQuorum(user) {
    // console.log(user);
    append(quorumDiv, $(`<li data-id="${user.id}">${user.name}</li>`), true);
}

// ---- pauta.store ---- //
var pautaTemplate = $('#template-pauta').html().trim();
function adicionarPauta(pauta) {
    var novoDiv = buildarTemplate(pautaTemplate, pauta);

    enfileirar(() => append($('#accordAndamento'), novoDiv, true));
}

// ---- item.store ---- //
var itemTemplate = $('#template-item').html().trim();
var isAdmin = $('#template-item').data('admin');
var statusMap = {
    'Aberta': ['danger', 'lock', 'Encerrar'],
    'Fechada': ['success', 'lock-open', 'Liberar Item'],
    'Encerrada': ['warning', 'exclamation-circle', 'Encerrada']
};
function adicionarItem(item) {
    item['admin'] = isAdmin ? 'block' : 'none';
    item['class'] = item.status == 'Aberta' ? 'aberto' : 'fechado';
    item['btn'] = statusMap[item.status][0];
    item['fa'] = statusMap[item.status][1];
    item['title'] = statusMap[item.status][2];

    if (item.status == 'Aberta') {
        item['status-class'] = 'btn-aberto';
    }
    else if (item.status == 'Fechada') {
        item['status-class'] = 'btn-fechado';
    }
    else {
        item['status-class'] = 'btn-encerrado';
    }

    var novoDiv = buildarTemplate(itemTemplate, item);
    var existente = $('.pauta-item[data-id="' + item.id + '"]');

    if (item.status == 'Aberta') {
        loadTexto(item);
    }

    if (existente.length) {
        enfileirar(() => replace(existente, novoDiv, null, true));
    } else {
        enfileirar(() => {
            var target = $('.pauta-items[data-id="' + item.pauta_id + '"]');
            append(target, novoDiv, true);
        });
    }
}

var textoTemplate = $('#template-texto').html().trim();
var textosDiv = $('#accordTexto');
function loadTexto(item) {
    var novoDiv = buildarTemplate(textoTemplate, item);
    var existente = $('#texto' + item.id);

    if (item._votoUsuario) {
        novoDiv.find('.voto-pendente').remove();
    } else {
        novoDiv.find('.voto-realizado').hide();
    }

    if (existente.length) {
        enfileirar(() => replace(existente, novoDiv, null, true));
    } else {
        enfileirar(() => append(textosDiv, novoDiv));
    }
}

// ---- item.update ---- //
function atualizarItem(item) {
    adicionarItem(item);
}

// ---- voto.store ---//
function adicionarVoto(voto) {
    let pautaDiv = $('#texto' + voto.item_id);
    let votosDiv = pautaDiv.find('.voto-realizado');
    let opcaoDiv = voto.voto == 0 ? pautaDiv.find('.voto-nao')
                    : voto.voto == 1 ? pautaDiv.find('.voto-sim')
                    : pautaDiv.find('.voto-abs');
    let numDiv = opcaoDiv.find('.voto-num');
    let targetDiv = opcaoDiv.find('.voto-users');

    let adendoHtml = '';
    if (voto.adendo && voto.voto == 0) { // 0 = não
        adendoHtml = `<div class="voto-adendo" title="Ler adendo">
            <i class="far fa-comment-alt"></i>
            <span class="ver-adendo" style="display:none">${voto.adendo}</span>
        </div>`;
    }

    let novoDiv = $(`<div class="voto" data-id="${voto.user_id}">${voto.user_nome}${adendoHtml}</div>`);
    let existente = targetDiv.find('.voto[data-id="' + voto.user_id + '"]');

    if (existente.length) {
        // não deveria ocorrer. location.reload()?
    } else {
        enfileirar(() => append(targetDiv, novoDiv, true));
    }

    enfileirar(() => {
        let numero = targetDiv.find('.voto').length;
        numDiv.html(numero + '');
    });
}

// ---- reuniao.status ---- //
let timerWrapper = $('#timer-wrapper');
let timer = $('#reuniao-timer');
let iniciarReuniao = $('#iniciar-reuniao');
let finalizarReuniao = $('#finalizar-reuniao');

let reuniaoTime = null; // start in millis
let lastIntervalId = null;

function atualizarStatus(reuniao) {
    if (lastIntervalId) {
        clearInterval(lastIntervalId);
    }

    if (reuniao.status) {
        timerWrapper.removeClass('iniciar');
        reuniaoTime = reuniao.aberto * 1000;
        lastIntervalId = setInterval(incTimer, 1000);
    } else {
        timerWrapper.addClass('iniciar');
        timer.html(genTime(reuniao.delta));
    }
}

function incTimer() {
    let delta = Date.now() - reuniaoTime;

    let totalSeconds = delta / 1000;
    let timeStr = genTime(totalSeconds);

    timer.html(timeStr);
}

function genTime(totalSeconds) {
    let hours = Math.floor(totalSeconds / 3600);
    totalSeconds %= 3600;
    let minutes = Math.floor(totalSeconds / 60);
    let seconds = Math.floor(totalSeconds % 60);

    return padLeft(hours + '') + ':' + padLeft(minutes + '') + ':' + padLeft(seconds + '');
}

function padLeft(str) {
    if (str.length < 2)
        return '0' + str;
    return str;
}

//:: Procedimentos :://

function append(parent, child, showEffect) {
    if (showEffect)
        child.hide();

    parent.append(child);

    if (showEffect)
        child.show('slow');
}

function replace(existente, novo, parent, showEffect) {
    existente.replaceWith(novo);

    if (showEffect) {
        // TODO melhorar esse efeito... (usar algum 'highlight' em vez de show(slow))
        novo.hide();
        novo.show('slow');
    }
}

setTimeout(() => {
    replace($(quorumDiv.children()[0]), $(`<li>Hey</li>`), quorumDiv, true);
}, 1000);

//:: Votação :://

$('.voto-adendo').prop('disabled', true);

$('input[type=radio][name=voto]').change(function() {
    $(this).closest('form').find('.voto-adendo').prop('disabled', $(this).val() == 1);
});

$('.voto-form').submit(function(e) {
    e.preventDefault();
    var form = $(this);

    var url = form.attr('action');
    var id = form.find('[name=item_pauta_id]').val();
    var voto = form.find('[name=voto]').val();
    var adendo = form.find('[name=adendo]').val();

    $.ajax({
        method: "POST",
        url: url,
        data: { item_pauta_id: id, voto: voto, adendo: adendo }
    })
    .done(function(res) {
        swal("Sucesso", res, "success");

        form.find("input").prop("disabled", true);
        form.find("textarea").prop("disabled", true);
        form.find("button").hide();
    })
    .fail(function(jqxhr, msg) {
        swal("Erro", jqxhr.responseText, "error");
    });
});

//:: Utilidades :://

function onFormSubmit(frm, ev, onComplete) {
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data) {
        },
        error: function (err, xhr) {
            console.log(xhr)
        },
        complete: function () {
            if (onComplete)
                onComplete();
        }
    });
    ev.preventDefault();
}

function submitForm(frm, onComplete) {
    frm.submit(function (ev) {
        onFormSubmit(frm, ev, onComplete);
    });
}

//:: Adicionar Pauta :://

let addItem = $('#addPauta');
let pautaDiv = $('#addPautaDiv');
let pautaForm = $('#addPautaForm');
addItem.on('click', function () {
    pautaDiv.modal('show')
});

submitForm(pautaForm, function() {
    $('#nome').val('');
    pautaDiv.modal('hide');
});

//:: Adicionar Item :://

let itemDiv = $('#addItemDiv');
let itemPautaId = itemDiv.find('.pauta-id'); // input
let itemPautaNome = itemDiv.find('.pauta-nome'); // span
let itemForm = $('#addItemForm');

$(document).on('click', '.add-item', function() {
    itemForm.find('input').val('');
    itemForm.find('textarea').val('');
    itemPautaId.val($(this).data('id'));
    itemPautaNome.html($(this).data('nome'));
    itemDiv.modal('show');
});

submitForm(itemForm, function() {
    itemDiv.modal('hide');
});

//:: Atualizar Item :://

$(document).on('click', '.btn-fechado', function(e) {
    e.preventDefault();
    $.post('/liberarItem/' + $(this).data('id'));
});

$(document).on('click', '.btn-aberto', function(e) {
    e.preventDefault();
    $.post('/encerrarItem/' + $(this).data('id'));
});

$(document).on('click', '.btn-encerrado', function(e) {
    e.preventDefault();
});

//:: Realizar Voto :://

$(document).on('submit', '.voto-form', function(e) {
    onFormSubmit($(this), e, () => {
        let divPendente = $(this).closest('.voto-pendente');
        let divRealizado = $(this).closest('.item-conteudo').find('.voto-realizado');

        divPendente.hide('slow');
        divRealizado.show('slow');
    });
});

//:: Ler Adendo :://

$(document).on('click', '.voto-adendo', function(e) {
    e.preventDefault();
    let adendo = $(this).children('.ver-adendo').html();

    if (adendo) {
        swal(adendo);
    }
});

$.post('/reunioes/' + reuniaoID + '/checar', data => {
    if (data) {
        data = JSON.parse(data);
        atualizarStatus(data);
    }
});

//:: Abrir / Fechar reunião :://

iniciarReuniao.click(e => {
    e.preventDefault();
    $.post('/reunioes/' + reuniaoID + '/abrir');
    timer.html('00:00:00');
});

finalizarReuniao.click(e => {
    e.preventDefault();
    $.post('/reunioes/' + reuniaoID + '/encerrar');
});
