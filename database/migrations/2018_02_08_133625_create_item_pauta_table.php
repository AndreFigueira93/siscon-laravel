<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemPautaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_pauta', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('pauta_id')->unsigned();
            $table->string('nome');
            $table->enum('status', ['Fechada', 'Aberta', 'Encerrada']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_pauta');
    }
}
