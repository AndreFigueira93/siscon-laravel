<?php

use App\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('email')->unique();
            $table->string('telefone')->nullable();
            $table->string('usuario')->unique();
            $table->string('foto')->nullable();
            $table->integer('orgao_id')->nullable()->default('1');
            $table->boolean('status')->default(0);
            $table->boolean('ativo')->default(1);
            $table->boolean('nivel')->default(1);
            $table->string('password');

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });

        User::create([
            'name' => 'Administrador',
            'usuario' => 'admin',
            'email' => 'admin@user.com',
            'password' => 'secret',
            'nivel' => 0
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
