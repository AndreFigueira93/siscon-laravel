<?php

namespace App\Models;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Database\Eloquent\Model;

class ReuniaoUser extends Model implements ShouldBroadcast
{
    protected $table = 'reunioes_users';

    protected $fillable = [
        'reuniao_id',
        'user_id'
    ];

    public function reuniao()
    {
        return $this->belongsTo('App\Models\Reuniao');
    }

    public function usuario()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return PrivateChannel
     */
    public function broadcastOn()
    {
        return new PrivateChannel('user.'.$this->update);
    }
}
