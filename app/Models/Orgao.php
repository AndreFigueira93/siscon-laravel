<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orgao extends Model
{
    protected $fillable = [
      'nome'
    ];

    public function usuarios()
    {
        return $this->hasMany('App\Models\User', 'orgao_id');
    }

}
