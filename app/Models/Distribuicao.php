<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Distribuicao extends Model
{
    protected $table = "distribuicoes";

    protected $fillable = [
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo("App\Models\User");
    }

}
