<?php

namespace App\Models;

use App\Models\Voto;
use Illuminate\Database\Eloquent\Model;

class ItemPauta extends Model
{
    protected $fillable = [
        'pauta_id',
        'nome',
        'status',
        'texto'
    ];

    protected $table = 'item_pauta';

    public function pauta()
    {
        return $this->belongsTo('App\Models\Pauta');
    }

    public function votos()
    {
        return $this->hasMany('App\Models\Voto');
    }

    /**
     * Retorna o voto feito pelo usuário de id $user_id, nulo se não há voto
     */
    public function votoUsuario($user_id)
    {
        return Voto::where([
            ['item_pauta_id', $this->attributes['id']],
            ['user_id', $user_id]
        ])->first();
    }
}
