<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Reuniao extends Model
{
    const STATUS_ABERTO = 1;
    const STATUS_FECHADO = 0;

    protected $table = 'reunioes';

    protected $fillable = [
        'descricao',
        'data',
        'status'
    ];
    protected $casts = [
        'data' => 'date:d-m-Y',
        'status' => 'boolean'
    ];

    protected $dates = [
        'data',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function dataExtensa($value)
    {
        return ucfirst(Carbon::createFromTimestampUTC(strtotime($value))->formatLocalized('%A, %d de %B de %Y'));
    }

    public function quorum()
    {
        return $this->belongsToMany('App\Models\ReuniaoUser', 'reunioes_users', 'id', 'reuniao_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'reunioes_users');
    }

    public function pautas()
    {
        return $this->hasMany('App\Models\Pauta');
    }

    public function ata()
    {
        return false;
    }

    public function aberto()
    {
        return $this->attributes['status'] == STATUS_ABERTO;
    }
}
