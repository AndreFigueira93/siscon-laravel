<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pauta extends Model
{
    protected $fillable = [
        'reuniao_id',
        'nome',
    ];

    public function reuniao()
    {
        return $this->belongsTo('App\Models\Reuniao');
    }

    public function items()
    {
        return $this->hasMany('App\Models\ItemPauta');
    }
}
