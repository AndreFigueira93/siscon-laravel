<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voto extends Model
{
    const VOTO_SIM = 1;
    const VOTO_NAO = 0; // Não com adendo = VOTO_NAO == 0 && adendo != null

    protected $fillable = [
        'item_pauta_id',
        'user_id',
        'voto',
        'adendo'
    ];

    public function itemPauta()
    {
        return $this->belongsTo('App\Models\ItemPauta');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
