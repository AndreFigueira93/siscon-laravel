<?php

namespace App\Events;

class ItemStoreEvent extends PusherEvent
{
    private $reuniao_id;

    public $pauta_id;
    public $id;
    public $nome;
    public $status;

    public function __construct($item)
    {
        $this->reuniao_id = $item->pauta->reuniao_id;
        $this->pauta_id = $item->pauta_id;
        $this->id = $item->id;
        $this->nome = $item->nome;
        $this->status = $item->status;
    }

    public function broadcastAs()
    {
        return 'item.store';
    }

    public function broadcastOn()
    {
        return ['reuniao-' . $this->reuniao_id];
    }
}
