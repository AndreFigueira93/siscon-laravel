<?php

namespace App\Events;

class UserJoinEvent extends PusherEvent
{
    private $reuniao_id;

    public $id;
    public $name;

    public function __construct($reuniao_id, $user)
    {
        $this->reuniao_id = $reuniao_id;

        $this->id = $user->id;
        $this->name = $user->name;
    }

    public function broadcastAs()
    {
        return 'user.join';
    }

    public function broadcastOn()
    {
        return ['reuniao-' . $this->reuniao_id];
    }
}
