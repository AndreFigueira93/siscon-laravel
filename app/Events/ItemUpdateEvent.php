<?php

namespace App\Events;

class ItemUpdateEvent extends PusherEvent
{
    private $reuniao_id;

    public $pauta_id;
    public $id;
    public $nome;
    public $status;
    public $texto; // Opcional

    public function __construct($item)
    {
        $this->reuniao_id = $item->pauta->reuniao_id;
        $this->pauta_id = $item->pauta_id;
        $this->id = $item->id;
        $this->nome = $item->nome;
        $this->status = $item->status;

        if ($item->status == 'Aberta') {
            $this->texto = $item->texto;
        }
    }

    public function broadcastAs()
    {
        return 'item.update';
    }

    public function broadcastOn()
    {
        return ['reuniao-' . $this->reuniao_id];
    }
}
