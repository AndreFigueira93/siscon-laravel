<?php

namespace App\Events;

class ReuniaoStatusEvent extends PusherEvent
{
    private $reuniao_id;

    public $status;
    public $aberto;
    public $encerrado;
    public $delta; // encerrado - aberto

    public function __construct($reuniao)
    {
        $this->reuniao_id = $reuniao->id;
        $this->status = $reuniao->status;

        $this->aberto = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s' , $reuniao->hora_abertura)->timestamp;

        if ($reuniao->hora_encerramento) {
            $this->encerrado = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s' , $reuniao->hora_encerramento)->timestamp;
            $this->delta = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s' , $reuniao->hora_encerramento)
                ->diffInSeconds(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s' , $reuniao->hora_abertura));
        } else {
            $this->encerrado = 0;
        }
    }

    public function broadcastAs()
    {
        return 'reuniao.status';
    }

    public function broadcastOn()
    {
        return ['reuniao-' . $this->reuniao_id];
    }
}
