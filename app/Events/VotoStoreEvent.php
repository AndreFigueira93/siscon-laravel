<?php

namespace App\Events;

class VotoStoreEvent extends PusherEvent
{
    private $reuniao_id;

    public $id;
    public $item_id;
    public $user_id;
    public $user_nome;
    public $voto;
    public $adendo;

    public function __construct($voto)
    {
        $this->reuniao_id = $voto->itemPauta->pauta->reuniao_id;

        $this->id = $voto->id;
        $this->item_id = $voto->item_pauta_id;
        $this->user_id = $voto->user_id;
        $this->user_nome = $voto->user->name;
        $this->voto = $voto->voto;
        $this->adendo = $voto->adendo;
    }

    public function broadcastAs()
    {
        return 'voto.store';
    }

    public function broadcastOn()
    {
        return ['reuniao-' . $this->reuniao_id];
    }
}
