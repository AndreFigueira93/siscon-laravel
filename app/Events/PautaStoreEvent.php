<?php

namespace App\Events;

class PautaStoreEvent extends PusherEvent
{
    private $reuniao_id;

    public $id;
    public $nome;

    public function __construct($pauta)
    {
        $this->reuniao_id = $pauta->reuniao_id;
        $this->id = $pauta->id;
        $this->nome = $pauta->nome;
    }

    public function broadcastAs()
    {
        return 'pauta.store';
    }

    public function broadcastOn()
    {
        return ['reuniao-' . $this->reuniao_id];
    }
}
