<?php

namespace App\Http\Controllers;

use \App\Models\Reuniao;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reunioes = Reuniao::all()->reverse();

        return view('home', compact('reunioes'));
    }
}
