<?php

namespace App\Http\Controllers;

use App\Models\Reuniao;
use App\Models\ItemPauta;
use App\Models\Voto;
use App\Models\ReuniaoUser;
use App\Models\User;

use Illuminate\Http\Request;
use Auth;

class ReuniaoController extends Controller
{
    public function index()
    {
        return view('reuniao.index');
    }

    public function loadJSON(Reuniao $reuniao)
    {
        $reuniao['pautas'] = $reuniao->pautas;
        foreach($reuniao['pautas'] as $pauta) {
            $pauta['items'] = $pauta->items;

            foreach($pauta['items'] as $item) {
                $item->_votoUsuario = $item->votoUsuario(Auth::user()->id);
            }
        }

        return $reuniao->toJson();
    }

    public function votosJSON(ItemPauta $item)
    {
        $votos = $item->votos;
        foreach($votos as $voto) {
            $voto->user_nome = $voto->user->name;
            $voto->item_id = $voto->item_pauta_id;
        }

        return $votos->toJson();
    }

    public function participar(Reuniao $reuniao)
    {
        if (!$reuniao) {
            return redirect('/'); // TODO error handling melhor
        }

        if ($reuniao->status == Reuniao::STATUS_FECHADO && Auth::user()->nivel == 1) {
            return redirect()->route('inicio')->with('error', 'A Reunião está fechada!');
        }



        $registro = ReuniaoUser::where([
            ['user_id', Auth::user()->id],
            ['reuniao_id', $reuniao->id]
        ])->first();

        if (!$registro) {
            ReuniaoUser::create([
                'user_id' => Auth::user()->id,
                'reuniao_id' => $reuniao->id
            ]);

            event(new \App\Events\UserJoinEvent($reuniao->id, Auth::user()));
        }

        return view('reuniao.participar', [
            'reuniao' => $reuniao
        ]);
    }

    public function votar(Request $request)
    {
        $item = ItemPauta::find($request->input('item_pauta_id'));

        if (!$item) {
            // Item não existe
            return response('Item expirado ou erro desconhecido', 500);
        }

        if ($item->status != 'Aberta') {
            // Item não está aberto a votos
            return response('Item fechado ou erro desconhecido', 403);
        }

        if ($item->votoUsuario(Auth::user()->id)) {
            return response('Voto já realizado neste item. Por favor atualize a página e tente novamente', 403);
        }

        $voto = new Voto($request->all());
        $voto->user_id = Auth::user()->id;

        if (!$voto->save()) {
            return response('Falha ao cadastrar voto. Por favor atualize a página e tente novamente', 500);
        }

        event(new \App\Events\VotoStoreEvent($voto));
        return response('Voto realizado com sucesso', 201);
    }

    function quorumCount($reuniao_id)
    {
        return ReuniaoUser::where([
            ['reuniao_id', $reuniao_id]
        ])->count();
    }

    function quorumUsers($reuniao_id)
    {
        $reuniaoUsers = ReuniaoUser::where([
            ['reuniao_id', $reuniao_id]
        ])->get();

        $res = [];

        foreach ($reuniaoUsers as $ru) {
            $user = User::find($ru->user_id);

            if (!$user)
                continue;

            $res[] = [
                "id" => $user->id,
                "name" => $user->name
            ];
        }

        return $res;
    }

    function abrir(Reuniao $reuniao)
    {
        $reuniao->status = 1;
        $reuniao->hora_encerramento = null;
        $reuniao->hora_abertura = date('Y-m-d H:i:s');

        if (!$reuniao->save()) {
            return response('Falha ao cadastrar reunião. Por favor atualize a página e tente novamente', 500);
        }

        event(new \App\Events\ReuniaoStatusEvent($reuniao));
    }

    function encerrar(Reuniao $reuniao)
    {
        $reuniao->status = 0;
        $reuniao->hora_encerramento = date('Y-m-d H:i:s');

        if (!$reuniao->save()) {
            return response('Falha ao cadastrar reunião. Por favor atualize a página e tente novamente', 500);
        }

        event(new \App\Events\ReuniaoStatusEvent($reuniao));
    }

    /**
     * Simula o ReuniaoStatusEvent... corrigir um bug estranho onde o pusher falha com várias abas abertas
     */
    function checar(Reuniao $reuniao)
    {
        $res = (object)[];
        $res->reuniao_id = $reuniao->id;
        $res->status = $reuniao->status;

        $res->aberto = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s' , $reuniao->hora_abertura)->timestamp;

        if ($reuniao->hora_encerramento) {
            $res->encerrado = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s' , $reuniao->hora_encerramento)->timestamp;
            $res->delta = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s' , $reuniao->hora_encerramento)
                ->diffInSeconds(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s' , $reuniao->hora_abertura));
        } else {
            $res->encerrado = 0;
        }

        return json_encode($res);
    }
}
