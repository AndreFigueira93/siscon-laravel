<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    protected function credentials(Request $request)
    {
        return ['usuario' => $request->{$this->username()}, 'password' => $request->password, 'ativo' => 1];
    }

    public function authenticated(Request $request, $user)
    {
        $user->update(['status' => 1]);
    }

    public function logout(Request $request)
    {
        $user = \Auth::user();
        $user->update(['status' => 0]);

        $this->guard()->logout();
        $request->session()->invalidate();

        return redirect('/');
    }

}
