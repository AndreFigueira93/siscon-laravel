<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Barryvdh\Elfinder\Connector;
use Barryvdh\Elfinder\ElfinderController;

class NovoElfinderController extends ElfinderController
{
    function access($attr, $relpath) {
        return strlen($relpath) !== 1 ? !($attr == 'read' || $attr == 'write') : null;
    }

    public function showConnectorext()
    {
        $user = Auth::user();
        $access = $this->app->config->get('elfinder.access');

        if ($user->nivel == 0) {
            $opts = [
                'roots' => [
                    [
                        'driver' => 'LocalFileSystem',
                        'path' => "arquivos",
                        'URL' => url('arquivos'),
                        'accessControl' => $access
                    ]
                ]
            ];
        } else {
            $opts = [
                'roots' => [
                    [
                        'driver' => 'LocalFileSystem',
                        'path' => "arquivos/Reunioes",
                        'URL' => url("arquivos/Reunioes"),
                        'defaults'   => array('read' => true, 'write' => false),
                        'accessControl' => $access
                    ]
                ]
            ];
        }

        $connector = new Connector(new \elFinder($opts));
        $connector->run();
        return $connector->getResponse();
    }
}
