<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Reuniao;
use Illuminate\Http\Request;

class ReuniaoCrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reunioes = Reuniao::all()->reverse();
        return view('admin.reunioes.index', compact('reunioes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.reunioes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = \Validator::make($request->toArray(), [
            'descricao' => 'required',
            'data' => 'required|date',
            'status' => 'required'
        ], [
            'descricao.required' => 'Por favor, digite uma descrição',
            'data.required' => 'Por favor, escolha uma data',
            'data.date' => 'Data inválida',
            'status.required' => 'Por favor, selecione o status'
        ]);

        if ($validator->invalid()) {
            return redirect()->route('reunioes.create')->withErrors($validator->errors());
        }
        $create = Reuniao::create($request->all());
        return redirect()->route('reunioes.index')->with('sucesso', 'Reunião criada com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reuniao  $reunio
     * @return \Illuminate\Http\Response
     */
    public function show(Reuniao $reunio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reuniao  $reunio
     * @return \Illuminate\Http\Response
     */
    public function edit(Reuniao $reunio)
    {
        return view('admin.reunioes.edit', compact('reunio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Reuniao  $reunio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reuniao $reunio)
    {
        $validator = \Validator::make($request->toArray(), [
            'descricao' => 'required',
            'data' => 'required|date',
            'status' => 'required'
        ], [
            'descricao.required' => 'Por favor, digite uma descrição',
            'data.required' => 'Por favor, escolha uma data',
            'data.date' => 'Data inválida',
            'status.required' => 'Por favor, selecione o status'
        ]);

        if ($validator->invalid()) {
            return redirect()->route('reunioes.create')->withErrors($validator->errors());
        }
        $create = Reuniao::create($request->all());
        return redirect()->route('reunioes.index')->with('sucesso', 'Reunião criada com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reuniao  $reunio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reuniao $reunio)
    {
        $tchau = $reunio->delete();
        if ($tchau) {
            return redirect()->route('reunioes.index')->with('sucesso', 'Reunião removida com sucesso!');
        } else {
            return redirect()->back()->with('invalido', 'Esta operação não pôde ser realizada no momento');
        }
    }
}
