<?php

namespace App\Http\Controllers\Admin;

use App\Models\Pauta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PautaCrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pauta = new Pauta($request->all());
        $add = $pauta->save();

        if ($add) {
            $reuniao_id = $request->input('reuniao_id');
            $titulo = $request->input('nome');

            event(new \App\Events\PautaStoreEvent($pauta));
            return response('success', 200);
        }

        return response('error', 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pauta  $pauta
     * @return \Illuminate\Http\Response
     */
    public function show(Pauta $pauta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pauta  $pauta
     * @return \Illuminate\Http\Response
     */
    public function edit(Pauta $pauta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pauta  $pauta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pauta $pauta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pauta  $pauta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pauta $pauta)
    {
        //
    }
}
