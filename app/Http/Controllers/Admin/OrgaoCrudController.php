<?php

namespace App\Http\Controllers\Admin;

use App\Models\Orgao;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrgaoCrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orgaos = Orgao::all();
        return view('admin.orgaos.index', compact('orgaos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.orgaos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->toArray(), [
            'nome' => 'required'
        ], [
            'nome.required' => 'Por favor, digite o nome do Órgão'
        ]);

        if ($validator->valid()) {
            $create = Orgao::create($request->all());
            return redirect()->route('entidades.index')->with('sucesso', 'Órgão criado com sucesso!');
        }
        return redirect()->route('entidades.index')->withErrors($validator->errors());
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $membros = User::all()->where('orgao_id', $id);
        return view('admin.orgaos.show', compact('membros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Orgao $orgao
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $orgao = Orgao::find($id);
        return view('admin.orgaos.edit', compact('orgao'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Orgao $entidade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Orgao $entidade)
    {
        $validator = \Validator::make($request->toArray(), [
            'nome' => 'required'
        ], [
            'nome.required' => 'Por favor, digite o nome do Órgão'
        ]);

        if (!$validator->fails()) {
            $entidade->update($request->all());
            return redirect()->route('entidades.index')->with('sucesso', 'Órgão atualizado com sucesso!');
        } else {
            return redirect()->back()->withErrors($validator->errors());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Orgao $entidade
     * @return \Illuminate\Http\Response
     */
    public function destroy(Orgao $entidade)
    {
        $tchau = $entidade->delete();
        if ($tchau) {
            return redirect()->route('entidades.index')->with('sucesso', 'Órgão removido com sucesso!');
        } else {
            return redirect()->back()->with('invalido', 'Esta operação não pôde ser realizada no momento');
        }
    }
}
