<?php

namespace App\Http\Controllers\Admin;

use App\Models\ReuniaoUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReunioesUsersCrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReuniaoUser  $reuniaoUser
     * @return \Illuminate\Http\Response
     */
    public function show(ReuniaoUser $reuniaoUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReuniaoUser  $reuniaoUser
     * @return \Illuminate\Http\Response
     */
    public function edit(ReuniaoUser $reuniaoUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ReuniaoUser  $reuniaoUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReuniaoUser $reuniaoUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReuniaoUser  $reuniaoUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReuniaoUser $reuniaoUser)
    {
        //
    }
}
