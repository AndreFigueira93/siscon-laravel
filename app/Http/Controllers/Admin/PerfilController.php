<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Auth;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PerfilController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param $id
     */
    public function perfil()
    {
        $user = \Auth::user();
        return view('admin.usuarios.perfil', compact('user'));
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     * @internal param User $user
     */
    public function perfilUpdate(Request $request)
    {
        $usuario = User::findOrFail(Auth::user()->id);
        $validator = \Validator::make($request->toArray(), [
            'name' => 'required',
            'usuario' => 'required',
            'email' => 'required',
            'telefone' => 'nullable|min:11|max:11'
        ], [
            'name.required' => 'Por favor, verifique o nome',
            'usuario.required' => 'Por favor, verifique o nome de usuário',
            'email.required' => 'Por favor, verifique o e-mail digitado',
        ]);

        if (!$validator->fails()) {
            $usuario->update($request->all());
            return redirect()->route('perfil')->with('sucesso', 'Usuário atualizado com sucesso!');
        } else {
            return redirect()->back()->withErrors($validator->errors());
        }
    }

    public function resetSenha(Request $request)
    {
        if (!(Hash::check($request->get('atual'), Auth::user()->password))) {
            return redirect()->back()->with("invalido","Senha atual inválida, por favor tente novamente.");
        }

        if(strcmp($request->get('atual'), $request->get('password')) == 0){
            return redirect()->back()->with("invalido","Sua nova senha não pode ser idêntica a senha atual, por favor tente novamente.");
        }

        $request->validate([
            'atual' => 'required',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = Auth::user();
        $user->update(['password' => $request->get('password')]);

        return redirect()->back()->with("sucesso","Senha alterada com sucesso!");
    }

    public function alterFoto(Request $request)
    {
        $user = Auth::user();
        $user->update(['foto' => $request->foto]);
        return back()->with('sucesso','Foto alterada com sucesso');
    }
}
