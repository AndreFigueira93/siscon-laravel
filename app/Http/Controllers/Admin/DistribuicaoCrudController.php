<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DistribuicaoCrudController extends Controller
{

    public $users;

    public function __construct()
    {
        $this->users = User::all()->where('id', '>', 4);
    }

    public function index()
    {
        return view('admin.distribuicoes.index');
    }

    public function create(Request $request)
    {
        $user = $this->users;
        $let = $user->random();
        return view('admin.distribuicoes.index', compact('let'));
    }
}
