<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Orgao;
use App\Models\User;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UsuarioCrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::all();
        return view('admin.usuarios.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $orgaos = Orgao::all();
        return view('admin.usuarios.create', compact('orgaos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->toArray(), [
            'name' => 'required',
            'usuario' => 'required|unique:users',
            'email' => 'required|unique:users',
            'orgao_id' => 'required',
            'ativo' => 'required',
            'password' => 'required|min:6|confirmed',
        ], [
            'name.required' => 'Por favor, verifique o nome digitado',
            'usuario.required' => 'Por favor, verifique o usuário digitado',
            'usuario.unique' => 'Este usuário já existe',
            'email.required' => 'Por favor, digite um e-mail válido',
            'email.unique' => 'E-mail já registrado',
            'orgao_id.required' => 'Por favor, selecione um órgão da lista',
            'password.confirmed' => 'As senhas não coincidem',
            'password.min' => 'A senha deve possuir no mínimo seis caracteres',
        ]);

        if (!$validator->fails()) {
            User::create($request->all());
            return redirect()->route('usuarios.index')->with('sucesso', 'Usuário criado com sucesso!');
        } else {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param User $usr
     */
    public function edit($id)
    {
        $user = User::find($id);
        $orgaos = Orgao::all();
        return view('admin.usuarios.edit', compact('user', 'orgaos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $usuario
     * @return \Illuminate\Http\Response
     * @internal param User $user
     */
    public function update(Request $request, User $usuario)
    {
        $validator = \Validator::make($request->toArray(), [
            'name' => 'required',
            'usuario' => 'required',
            'email' => 'required',
            'orgao_id' => 'required',
            'ativo' => 'required',
        ], [
            'name.required' => 'Por favor, verifique o campo :attribute',
            'usuario.required' => 'Por favor, verifique o campo :attribute',
            'email.required' => 'Por favor, verifique este campo',
            'orgao_id.required' => 'Por favor, verifique este campo',
            'orgao_id.in' => 'Por favor, selecione um orgão da lista'
        ]);

        if (!$validator->fails()) {
            $usuario->update($request->all());
            return redirect()->route('usuarios.index')->with('sucesso', 'Usuário atualizado com sucesso!');
        } else {
            return redirect()->back()->withErrors($validator->errors());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $usuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $usuario)
    {
        $usuario->update(['ativo' => 0]);
        $tchau = $usuario->delete();
        if ($tchau) {
            return redirect()->route('usuarios.index')->with('sucesso', 'Usuário removido com sucesso!');
        } else {
            return redirect()->back()->with('invalido', 'Esta operação não pôde ser realizada no momento');
        }
    }

    public function removidos()
    {
        $usuarios = User::onlyTrashed()->get();
        return view('admin.usuarios.removidos', compact('usuarios'));
    }

    public function restore($id)
    {
        $user = User::onlyTrashed()->where('id', $id)->first();
        $restore = $user->restore();

        if ($restore) {
            return redirect()->route('usuarios.index')->with('sucesso', 'Usuário restaurado com sucesso!');
        } else {
            return redirect()->back()->with('invalido', 'Esta operação não pôde ser realizada no momento');
        }
    }

    public function forceDelete($id)
    {
        $user = User::onlyTrashed()->where('id', $id)->first();
        $tchau = $user->forceDelete();

        if ($tchau) {
            return redirect()->route('usuarios.index')->with('sucesso', 'Usuário removido com sucesso!');
        } else {
            return redirect()->back()->with('invalido', 'Esta operação não pôde ser realizada no momento');
        }
    }


}
