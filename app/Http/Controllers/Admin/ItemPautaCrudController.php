<?php

namespace App\Http\Controllers\Admin;

use App\Models\ItemPauta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemPautaCrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = new ItemPauta($request->all());
        $item->status = 'Fechada';

        if ($item->save()) {
            event(new \App\Events\ItemStoreEvent($item));
        }
        else {
            // error
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ItemPauta  $itemPauta
     * @return \Illuminate\Http\Response
     */
    public function show(ItemPauta $itemPauta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ItemPauta  $itemPauta
     * @return \Illuminate\Http\Response
     */
    public function edit(ItemPauta $itemPauta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ItemPauta  $itemPauta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemPauta $itemPauta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ItemPauta  $itemPauta
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemPauta $itemPauta)
    {
        //
    }
}
