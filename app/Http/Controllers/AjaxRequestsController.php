<?php

namespace App\Http\Controllers;

use App\Models\Reuniao;
use App\Models\ReuniaoUser;
use App\Models\User;
use \App\Models\ItemPauta;
use Illuminate\Http\Request;

class AjaxRequestsController extends Controller
{
    public function getPautas($id)
    {
        $reuniao = \App\Models\Reuniao::all()->find($id);
        return view('reuniao.ajax.pautas', compact('reuniao'));
    }

    public function getUsers($id)
    {
        $reuniao = \App\Models\Reuniao::all()->find($id);
        return view('reuniao.ajax.quorum', compact('reuniao'));
    }

    public function getReunions()
    {
        $reunioes = \App\Models\Reuniao::all()->reverse();
        return view('reuniao.ajax.lista', compact('reunioes'));
    }

    public function delPauta($id)
    {
        $del = \App\Models\Pauta::all()->find($id)->delete();
        if ($del) {
            return response('success', 200);
        } else {
            return response('error', 403);
        }
    }

    public function addItemView($id)
    {
        $pauta = \App\Models\Pauta::all()->find($id);
        return view('reuniao.ajax.itemPautaModal', compact('pauta'));
    }

    public function liberarItem($id)
    {
        $item = ItemPauta::find($id);
        $change = $item->update(['status' => 'Aberta']);

        if ($change) {
            event(new \App\Events\ItemUpdateEvent($item));
            return response('ok', 200);
        }

        return response('error', 403);
    }

    public function encerrarItem($id)
    {
        $item = ItemPauta::find($id);
        $change = $item->update(['status' => 'Encerrada']);

        if ($change) {
            event(new \App\Events\ItemUpdateEvent($item));
            return response('ok', 200);
        }

        return response('error', 403);
    }

    public function historicoUser(User $user)
    {
        $reunioes = Reuniao::all();
        $find = ReuniaoUser::all();

        foreach ($reunioes as $r) {
            if ($find->where('reuniao_id', $r->id)->where('user_id', $user->id)->first()) {
                $info[] = [
                    'reuniao' => $r->descricao,
                    'data' => date('d-m-Y', strtotime($r->data)),
                    'status' => "Presente",
                    'icon' => "<i class='fa fa-check' style='color: green'></i> "
                ];
            } else {
                $info[] = [
                    'reuniao' => $r->descricao,
                    'data' => date('d-m-Y', strtotime($r->data)),
                    'status' => "Ausente",
                    'icon' => "<i class='fa fa-ban' style='color: red'></i> "
                ];
            }
        }

        return view('admin.usuarios.ajax.historico', compact('info', 'user'));
    }

    public function historicoReuniao(Reuniao $reuniao)
    {
        $users = User::all();
        $find = ReuniaoUser::all();

        foreach ($users as $r) {
            if ($find->where('user_id', $r->id)->where('reuniao_id', $reuniao->id)->first()) {
                $info[] = [
                    'usuario' => "$r->name",
                    'status' => "Presente",
                    'icon' => "<i class='fa fa-check' style='color: green'></i> "
                ];
            } else {
                $info[] = [
                    'usuario' => "$r->name",
                    'status' => "Ausente",
                    'icon' => "<i class='fa fa-ban' style='color: red'></i> "
                ];
            }
        }

        return view('admin.reunioes.ajax.presentes', compact('info', 'reuniao'));
    }

}
