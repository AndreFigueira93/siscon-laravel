<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Jenssegers\Date\Date;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Date::setLocale('pt-BR');
        setlocale(LC_ALL, 'pt_BR', 'pt-BR.utf-8', 'pt-BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Belem');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Date::setLocale('pt-BR');
        setlocale(LC_ALL, 'pt_BR', 'pt-BR.utf-8', 'pt-BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Belem');
    }
}
